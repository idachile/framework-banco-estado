<?php include 'header.php';?>
<main>
  <section class="content breadcrumbs-bar-frame">
    <?php include 'partials/breadcrumbs.php';?>
  </section>
  <section class="content">
  <div class="introduccion">
    <div class="title-section">
      <h1>Paleta de colores</h1>
    </div>
    <p>El uso del color en las distintas secciones y m&#243;dulos del sitio privado es fundamental. Permite mantener la identidad visual de BancoEstado y resaltar la ubicaci&#243;n de los elementos m&#225;s importantes de cada proceso. Otro de sus objetivos es guiar al usuario en toda operaci&#243;n que realice: por ejemplo, destacar el espacio destinado al ingreso de datos o mostrar el avance de una transacci&#243;n.</p>
  </div>
</section>
  <div class="content-box">
    <section>
      <p class="c">Estandarizar la gama de colores, y sus variantes, busca facilitar el diseño y programaci&#243;n de nuevos elementos. Los equipos de trabajo deben respetar esta pauta al momento de proyectar, desarrollar e implementar soluciones, las cuales no deben afectar la experiencia de los usuarios.</p>
      <div class="item">
        <div class="title-section">
          <h2>Color Principal</h2>
          <div class="divider"></div>
        </div>
        <p>Es el dominante en el sitio privado y sus diferentes secciones. Su presencia mantiene el sello de BancoEstado en cada m&#243;dulo y marca las acciones clave que un usuario puede realizar. Sus variantes entregan matices visualmente atractivas sin perder la identidad de marca.</p>
        <div class="colores">
          <div class="gridle-row">
            <div class="gridle-gr-3 content-color">
              <div class="color">
                <div class="color__paint color--main-darker"></div>
                <div class="color__info">
                  <p><b>color principal darker</b></p>
                  <p>#A35E00</p>
                  <p>rgb(163, 94, 0)</p>
                </div>
              </div>
            </div>
            <div class="gridle-gr-3 content-color">
              <div class="color">
                <div class="color__paint color--main-dark"></div>
                <div class="color__info">
                  <p><b>color principal dark</b></p>
                  <p>#E78400</p>
                  <p>rgb(231, 132, 133)</p>
                </div>
              </div>
            </div>
            <div class="gridle-gr-3 content-color">
              <div class="color color-principal">
                <div class="color__paint color--main-regular"></div>
                <div class="color__info">
                  <p><b>color principal regular</b></p>
                  <p>#F49600</p>
                  <p>rgb(244, 150, 0)</p>
                </div>
              </div>
            </div>
            <div class="gridle-gr-3 content-color">
              <div class="color">
                <div class="color__paint color--main-light"></div>
                <div class="color__info">
                  <p><b>color principal light</b></p>
                  <p>#F8AD38</p>
                  <p>rgb(248, 173, 56)</p>
                </div>
              </div>
            </div>
            <div class="gridle-gr-3 content-color">
              <div class="color">
                <div class="color__paint color--main-lighter"></div>
                <div class="color__info">
                  <p><b>color principal lighter</b></p>
                  <p>#FEF9F0</p>
                  <p>rgb(253, 242, 224)</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="item">
        <div class="title-section">
          <h2>Escala de Grises</h2>
          <div class="divider"></div>
        </div>
        <p>Su presencia en la paleta de colores entrega m&#250;ltiples posibilidades. Permite resaltar cifras, tablas o pequeñas acciones que un usuario puede desarrollar. Los diferentes textos, dependiendo su ubicaci&#243;n, tambi&#233;n deben responder a esta escala de grises.</p>
        <div class="colores">
          <div class="gridle-row">
            <div class="gridle-gr-3 content-color">
              <div class="color">
                <div class="color__paint color--black"></div>
                <div class="color__info">
                  <p><b>Negro</b></p>
                  <p>#000000</p>
                  <p>rgb(0, 0, 0)</p>
                </div>
              </div>
            </div>
            <div class="gridle-gr-3 content-color">
              <div class="color">
                <div class="color__paint color--grey-39"></div>
                <div class="color__info">
                  <p><b>color grey darker</b></p>
                  <p>#9B9B9B</p>
                  <p>rgb(155, 155, 155)</p>
                </div>
              </div>
            </div>
            <div class="gridle-gr-3 content-color">
              <div class="color">
                <div class="color__paint color--grey-19"></div>
                <div class="color__info">
                  <p><b>color grey dark</b></p>
                  <p>#D1D0D0</p>
                  <p>rgb(209, 208, 208)</p>
                </div>
              </div>
            </div>
            <div class="gridle-gr-3 content-color">
              <div class="color">
                <div class="color__paint color--grey-6"></div>
                <div class="color__info">
                  <p><b>color grey light</b></p>
                  <p>#F1F1F1</p>
                  <p>rgb(241, 241, 241)</p>
                </div>
              </div>
            </div>
            <div class="gridle-gr-3 content-color">
              <div class="color">
                <div class="color__paint color--white"></div>
                <div class="color__info">
                  <p><b>blanco</b></p>
                  <p>#ffffff</p>
                  <p>rgb(255, 155, 255)</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="item">
        <div class="title-section">
          <h2>Colores de Respaldo</h2>
          <div class="divider"></div>
        </div>
        <p>Al interior del sitio privado conviven elementos que deben ser destacados dependiendo de la acci&#243;n que realicen. Si por ejemplo se trata de procesos con m&#250;ltiples pasos, que deben mostrar su evoluci&#243;n, es posible utilizar una de las siguientes opciones y sus variantes.</p>
        <div class="colores">
          <div class="title-section">
            <h3>&#201;xito</h3>
          </div>
          <div class="divider"></div>
          <div class="gridle-row">
            <!--<div class="gridle-gr-3 content-color">
              <div class="color">
                <div class="color__paint color--success-darker"></div>
                <div class="color__info">
                  <p><b>color &#233;xito darker</b></p>
                  <p>#3C6A0F</p>
                  <p>rgb(60, 106, 15)</p>
                </div>
              </div>
            </div>-->
            <div class="gridle-gr-3 content-color">
              <div class="color">
                <div class="color__paint color--success-dark"></div>
                <div class="color__info">
                  <p><b>color &#233;xito dark</b></p>
                  <p>#53950F</p>
                  <p>rgb(83, 149, 15)</p>
                </div>
              </div>
            </div>
            <!--<div class="gridle-gr-3 content-color">
              <div class="color color-principal">
                <div class="color__paint color--success-regular"></div>
                <div class="color__info">
                  <p><b>color &#233;xito regular</b></p>
                  <p>#64A622</p>
                  <p>rgb(100, 166, 34)</p>
                </div>
              </div>
            </div>-->
            <!--<div class="gridle-gr-3 content-color">
              <div class="color">
                <div class="color__paint color--success-light"></div>
                <div class="color__info">
                  <p><b>color &#233;xito light</b></p>
                  <p>#86B954</p>
                  <p>rgb(134, 185, 84)</p>
                </div>
              </div>
            </div>-->
            <div class="gridle-gr-3 content-color">
              <div class="color">
                <div class="color__paint color--success-lighter"></div>
                <div class="color__info">
                  <p><b>color &#233;xito lighter</b></p>
                  <p>#F6F9F2</p>
                  <p>rgb(236, 245, 228)</p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="colores">
          <div class="title-section">
            <h3>Informaci&#243;n</h3>
         </div>
          <div class="divider"></div>
          <div class="gridle-row">
          <!--  <div class="gridle-gr-3 content-color">
              <div class="color">
                <div class="color__paint color--info-darker"></div>
                <div class="color__info">
                  <p><b>color informaci&#243;n darker</b></p>
                  <p>#005989</p>
                  <p>rgb(0, 89, 137)</p>
                </div>
              </div>
            </div>-->
            <div class="gridle-gr-3 content-color">
              <div class="color">
                <div class="color__paint color--info-dark"></div>
                <div class="color__info">
                  <p><b>color informaci&#243;n dark</b></p>
                  <p>#007DC1</p>
                  <p>rgb(0, 125, 193)</p>
                </div>
              </div>
            </div>
          <!--  <div class="gridle-gr-3 content-color">
              <div class="color color-principal">
                <div class="color__paint color--info-regular"></div>
                <div class="color__info">
                  <p><b>color informaci&#243;n regular</b></p>
                  <p>#0F90D0</p>
                  <p>rgb(15, 144, 208)</p>
                </div>
              </div>
            </div>-->
            <!--<div class="gridle-gr-3 content-color">
              <div class="color">
                <div class="color__paint color--info-light"></div>
                <div class="color__info">
                  <p><b>color informaci&#243;n ligth</b></p>
                  <p>#48A7DC</p>
                  <p>rgb(72, 167, 220)</p>
                </div>
              </div>
            </div>-->
            <div class="gridle-gr-3 content-color">
              <div class="color">
                <div class="color__paint color--info-lighter"></div>
                <div class="color__info">
                  <p><b>color informaci&#243;n lighter</b></p>
                  <p>#F1F8FC</p>
                  <p>rgb(227, 241, 250)</p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="colores">
          <div class="title-section">
            <h3>Error</h3>
         </div>
          <div class="divider"></div>
          <div class="gridle-row">
          <!--  <div class="gridle-gr-3 content-color">
              <div class="color">
                <div class="color__paint color--danger-darker"></div>
                <div class="color__info">
                  <p><b>color error darker</b></p>
                  <p>#781118</p>
                  <p>rgb(120, 17, 24)</p>
                </div>
              </div>
            </div>-->
            <div class="gridle-gr-3 content-color">
              <div class="color">
                <div class="color__paint color--danger-dark"></div>
                <div class="color__info">
                  <p><b>color error dark</b></p>
                  <p>#A9141E</p>
                  <p>rgb(169, 20, 30)</p>
                </div>
              </div>
            </div>
          <!--  <div class="gridle-gr-3 content-color">
              <div class="color color-principal">
                <div class="color__paint color--danger-regular"></div>
                <div class="color__info">
                  <p><b>color error regular</b></p>
                  <p>#B91928</p>
                  <p>rgb(185, 25, 40)</p>
                </div>
              </div>
            </div>-->
            <!--<div class="gridle-gr-3 content-color">
              <div class="color">
                <div class="color__paint color--danger-light"></div>
                <div class="color__info">
                  <p><b>color error light</b></p>
                  <p>#CA525B</p>
                  <p>rgb(202, 82, 91)</p>
                </div>
              </div>
            </div>-->
            <div class="gridle-gr-3 content-color">
              <div class="color">
                <div class="color__paint color--danger-lighter"></div>
                <div class="color__info">
                  <p><b>color error lighter</b></p>
                  <p>#FBF1F2</p>
                  <p>rgb(247, 228, 230)</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
<?php include 'footer.php';?>
</main>
