<div class="desplegables horizon">
  <div class="gridle-row">
    <div class="gridle-gr-12">
      <div class="desplegable__container" data-desplegable="desplegable">
        <div class="desplegable__head">
          <h4 class="desplegable__title">Html</h4>
          <div class="desplegable__button" data-desplegable="click">
            <i class="icon-elem icon-elem--arrow_drop_down" data-desplegable="icono"></i>
          </div>
        </div>
        <div class="desplegable__body" style="display: none;">
          <div class="code-box">
            <div class="code-box__button">
              <button id="buttonCopy" class="button code-box__copy" type="button" data-func="copiar"><span class="icon-elem icon-elem--content_copy"></span>Copiar</button>
            </div>
            <pre class="prettyprint linenums">
              <code class="lang-html">
 &#60;h1 class="titulo"&#62;T&#237;tulo Principal&#60;/h1&#62;
         </code>
            </pre>
          </div>
        </div>
      </div>
    </div>
    <div class="gridle-gr-12">
      <div class="desplegable__container" data-desplegable="desplegable">
        <div class="desplegable__head">
          <h4 class="desplegable__title">css</h4>
          <div class="desplegable__button" data-desplegable="click">
            <i class="icon-elem icon-elem--arrow_drop_down" data-desplegable="icono"></i>
          </div>
        </div>
        <div class="desplegable__body" style="display: none;">
          <div class="code-box">
            <div class="code-box__button">
              <button id="buttonCopy" class="button code-box__copy" type="button" data-func="copiar"><span class="icon-elem icon-elem--content_copy"></span>Copiar</button>
            </div>
            <pre class="prettyprint linenums">
              <code class="lang-css">
  .titulo{
  font-size: 40px;
  color: #333;
  font-family: 'Open-Sans', sans-serif;
  }
  div{
  color: #000;
  }            </code>
            </pre>
          </div>
        </div>
      </div>
    </div>
    <div class="gridle-gr-12">
      <div class="desplegable__container" data-desplegable="desplegable">
        <div class="desplegable__head">
          <h4 class="desplegable__title">JavaScript</h4>
          <div class="desplegable__button" data-desplegable="click">
            <i class="icon-elem icon-elem--arrow_drop_down" data-desplegable="icono"></i>
          </div>
        </div>
        <div class="desplegable__body" style="display: none;">
          <div class="code-box">
            <div class="code-box__button">
              <button id="buttonCopy" class="button code-box__copy" type="button" data-func="copiar"><span class="icon-elem icon-elem--content_copy"></span>Copiar</button>
            </div>
            <pre class="prettyprint linenums">
              <code class="lang-js">
function helloWorld(world) {
for (var i = 42; --i >= 0;) {
alert('Hello ' + String(world));
}
}
              </code>
            </pre>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
