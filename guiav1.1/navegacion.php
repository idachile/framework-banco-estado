<?php include 'header.php';?>
<main>
  <section class="content breadcrumbs-bar-frame">
    <?php include 'partials/breadcrumbs.php';?>
  </section>
  <section class="content">
  <div class="introduccion">
    <div class="title-section">
      <h1>Navegaci&#243;n</h1>
    </div>
    <p>Toda plataforma, para su utilizaci&#243;n, debe permitir una desplazamiento fluido por sus distintas secciones. La necesidad que todo usuario tiene por acceder r&#225;pidamente a todo el contenido hace necesario implementar elementos que cumplan esta funci&#243;n. Un men&#250; puede ser diseñado bajo distintos par&#225;metros visuales, aunque debe respetar el contexto (colores, tamaños, etc.) en el que est&#225; ubicado.</p>
  </div>
</section>
  <div class="content-box">
    <section>
      <div class="item">
        <div class="title-section">
          <h2>breadcrumbs</h2>
          <div class="divider"></div>
        </div>
        <p>Ubicado de preferencia en la parte superior de toda p&#225;gina, es la encargada de visualizar la secci&#243;n exacta en la que el usuario est&#225;. Identifica la pantalla en la que se est&#225; operando y desde donde proviene.</p>
        <div class="tipogafria">
          <div class="agrupador-contenido">
            <ul class="breadcrumb">
              <li class="breadcrumb__item">
                <a href="#">Inicio</a>
              </li>
              <li class="breadcrumb__item breadcrumb__item--activo">
                Pagos
              </li>
            </ul>
          </div>
          <div class="agrupador-contenido">
            <ul class="breadcrumb">
              <li class="breadcrumb__item">
                <a href="#">Inicio</a>
              </li>
              <li class="breadcrumb__item">
                <a href="#">Pagos</a>
              </li>
              <li class="breadcrumb__item breadcrumb__item--activo">
                Pago Cuentas de Servicios
              </li>
            </ul>
          </div>
          <div class="agrupador-contenido">
            <ul class="breadcrumb">
              <li class="breadcrumb__item">
                <a href="#">Inicio</a>
              </li>
              <li class="breadcrumb__item">
                <a href="#">Pagos</a>
              </li>
              <li class="breadcrumb__item">
                <a href="#">Pago Cuentas de Servicios</a>
              </li>
              <li class="breadcrumb__item breadcrumb__item--activo">
                Pagar Cuentas
              </li>
            </ul>
          </div>
        </div>
        <!--CODIGO-->
        <div class="wrapper">
          <ul class="tabs clearfix" data-tabgroup="breadcrumbs">
            <li><a href="#breadcrumbs-html"><span class="texto-desplegable">html</span><span class="icon-elem icon-elem--arrow_drop_down"></span></a></li>
            <li><a href="#breadcrumbs-css"><span class="texto-desplegable">css</span><span class="icon-elem icon-elem--arrow_drop_down"></span></a></li>
          </ul>
          <section id="breadcrumbs" class="tabgroup close">
            <div id="breadcrumbs-html">
              <div class="code-box">
                <pre id="copy-bread" class="prettyprint"><code class="lang-html">&#60;ul class="breadcrumb"&#62;
  &#60;li class="breadcrumb__item"&#62;
    &#60;a href="#"&#62;
      Inicio
    &#60;/a&#62;
  &#60;/li&#62;
  &#60;li class="breadcrumb__item breadcrumb__item--activo"&#62;
    Pagos
  &#60;/li&#62;
&#60;/ul&#62;<hr>
&#60;ul class="breadcrumb"&#62;
  &#60;li class="breadcrumb__item"&#62;
   &#60;a href="#"&#62;
     Inicio
   &#60;/a&#62;
  &#60;/li&#62;
  &#60;li class="breadcrumb__item"&#62;
   &#60;a href="#"&#62;
     Pagos
   &#60;/a&#62;
  &#60;/li&#62;
  &#60;li class="breadcrumb__item breadcrumb__item--activo"&#62;
   Pagos Cuentas de Servicios
  &#60;/li&#62;
&#60;/ul&#62;<hr>
&#60;ul class="breadcrumb"&#62;
  &#60;li class="breadcrumb__item"&#62;
   &#60;a href="#"&#62;
     Inicio
   &#60;/a&#62;
  &#60;/li&#62;
  &#60;li class="breadcrumb__item"&#62;
   &#60;a href="#"&#62;
     Pagos
   &#60;/a&#62;
  &#60;/li&#62;
  &#60;li class="breadcrumb__item"&#62;
   &#60;a href="#"&#62;
     Pagos Cuentas de Servicios
   &#60;/a&#62;
  &#60;/li&#62;
  &#60;li class="breadcrumb__item breadcrumb__item--activo"&#62;
   Pagar Cuentas
  &#60;/li&#62;
&#60;/ul&#62;</code></pre>
                <div class="code-box__button">
                  <button data-clipboard-target="#copy-bread" class="button code-box__copy" type="button"><span class="icon-elem icon-elem--content_copy"></span>Copiar</button>
                </div>
              </div>
            </div>
            <div id="breadcrumbs-css">
              <div class="code-box">
                <div class="css-modificadores">
                  <h6>Modificadores de estilos</h6>
                  <ul class="listado-clases">
                    <li><code class="clases">breadcrumb__item--activo</code></li>
                  </ul>
                </div>
              </div>
            </div>
          </section>
        </div>
        <!--CODIGO-->
      </div>
      <div class="item">
        <div class="title-section">
          <h2>Paginaci&#243;n</h2>
          <div class="divider"></div>
        </div>
        <p>Una buena experiencia de usuario implica ordenar los datos que se despliegan en el sitio privado de BancoEstado. Para revisar el detalle es posible implementar complementos de paginaci&#243;n, que permite desplazarse por el total de p&#225;ginas sin seguir un orden establecido.</p>
        <div class="tipogafria">
          <div class="agrupador-contenido">
            <nav>
              <ul class="paginacion">
                <li class="paginacion__item paginacion__item--desactivado"><a class="paginacion__link" href="#"><span class="icon-elem icon-elem--navigate_before"></span></a></li>
                <li class="paginacion__item paginacion__item--activo"><a class="paginacion__link" href="#">1</a></li>
                <li class="paginacion__item"><a class="paginacion__link" href="#">2</a></li>
                <li class="paginacion__item"><a class="paginacion__link" href="#">3</a></li>
                <li class="paginacion__item"><a class="paginacion__link" href="#">4</a></li>
                <li class="paginacion__item"><a class="paginacion__link" href="#">5</a></li>
                <li class="paginacion__item"><a class="paginacion__link" href="#"><span class="icon-elem icon-elem--navigate_next"></a></li>
              </ul>
            </nav>
          </div>
          <div class="agrupador-contenido">
            <nav>
              <ul class="paginacion">
                <li class="paginacion__item"><a class="paginacion__link" href="#"><span class="icon-elem icon-elem--navigate_before"></span></a></li>
                <li class="paginacion__item"><a class="paginacion__link" href="#">1</a></li>
                <li class="paginacion__item paginacion__item--activo"><a class="paginacion__link" href="#">2</a></li>
                <li class="paginacion__item"><a class="paginacion__link" href="#">3</a></li>
                <li class="paginacion__item"><a class="paginacion__link" href="#">4</a></li>
                <li class="paginacion__item"><a class="paginacion__link" href="#">5</a></li>
                <li class="paginacion__item"><a class="paginacion__link" href="#"><span class="icon-elem icon-elem--navigate_next"></a></li>
              </ul>
            </nav>
          </div>
          <div class="agrupador-contenido">
            <nav>
              <ul class="paginacion">
                <li class="paginacion__item"><a class="paginacion__link" href="#"><span class="icon-elem icon-elem--navigate_before"></span></a></li>
                <li class="paginacion__item"><a class="paginacion__link" href="#">1</a></li>
                <li class="paginacion__item"><a class="paginacion__link" href="#">2</a></li>
                <li class="paginacion__item"><a class="paginacion__link" href="#">3</a></li>
                <li class="paginacion__item"><a class="paginacion__link" href="#">4</a></li>
                <li class="paginacion__item paginacion__item--activo"><a class="paginacion__link" href="#">5</a></li>
                <li class="paginacion__item paginacion__item--desactivado"><a class="paginacion__link" href="#"><span class="icon-elem icon-elem--navigate_next"></a></li>
              </ul>
            </nav>
          </div>
        </div>
        <!--CODIGO-->
        <div class="wrapper">
          <ul class="tabs clearfix" data-tabgroup="breadcrumbs">
            <li><a href="#paginacion-html"><span class="texto-desplegable">html</span><span class="icon-elem icon-elem--arrow_drop_down"></span></a></li>
            <li><a href="#paginacion-css"><span class="texto-desplegable">css</span><span class="icon-elem icon-elem--arrow_drop_down"></span></a></li>
          </ul>
          <section id="paginacion" class="tabgroup close">
            <div id="paginacion-html">
              <div class="code-box">
                <pre id="copy-paginacion" class="prettyprint"><code class="lang-html">&#60;ul class="paginacion"&#62;
  &#60;li class="paginacion__item paginacion__item--desactivado"&#62;
    &#60;a href="#" class="paginacion__link"&#62;
      &#60;span class="icon-elem icon-elem--navigate_before"&#62;&#60;/span&#62;
    &#60;/a&#62;
  &#60;/li&#62;
  &#60;li class="paginacion__item paginacion__item--activo"&#62;
    &#60;a href="#" class="paginacion__link"&#62;
      1
    &#60;/a&#62;
  &#60;/li&#62;
  &#60;li class="paginacion__item"&#62;
    &#60;a href="#" class="paginacion__link"&#62;
      2
    &#60;/a&#62;
  &#60;/li&#62;
  &#60;li class="paginacion__item"&#62;
    &#60;a href="#" class="paginacion__link"&#62;
      3
    &#60;/a&#62;
  &#60;/li&#62;
  &#60;li class="paginacion__item"&#62;
    &#60;a href="#" class="paginacion__link"&#62;
      4
    &#60;/a&#62;
  &#60;/li&#62;
  &#60;li class="paginacion__item"&#62;
    &#60;a href="#" class="paginacion__link"&#62;
      5
    &#60;/a&#62;
  &#60;/li&#62;
  &#60;li class="paginacion__item"&#62;
    &#60;a href="#" class="paginacion__link"&#62;
      &#60;span class="icon-elem icon-elem--navigate_next"&#62;&#60;/span&#62;
    &#60;/a&#62;
  &#60;/li&#62;
&#60;/ul&#62;<hr>
&#60;ul class="paginacion"&#62;
  &#60;li class="paginacion__item"&#62;
    &#60;a href="#" class="paginacion__link"&#62;
      &#60;span class="icon-elem icon-elem--navigate_before"&#62;&#60;/span&#62;
    &#60;/a&#62;
  &#60;/li&#62;
  &#60;li class="paginacion__item"&#62;
    &#60;a href="#" class="paginacion__link"&#62;
      1
    &#60;/a&#62;
  &#60;/li&#62;
  &#60;li class="paginacion__item paginacion__item--activo"&#62;
    &#60;a href="#" class="paginacion__link"&#62;
      2
    &#60;/a&#62;
  &#60;/li&#62;
  &#60;li class="paginacion__item"&#62;
    &#60;a href="#" class="paginacion__link"&#62;
      3
    &#60;/a&#62;
  &#60;/li&#62;
  &#60;li class="paginacion__item"&#62;
    &#60;a href="#" class="paginacion__link"&#62;
      4
    &#60;/a&#62;
  &#60;/li&#62;
  &#60;li class="paginacion__item"&#62;
    &#60;a href="#" class="paginacion__link"&#62;
      5
    &#60;/a&#62;
  &#60;/li&#62;
  &#60;li class="paginacion__item"&#62;
    &#60;a href="#" class="paginacion__link"&#62;
      &#60;span class="icon-elem icon-elem--navigate_next"&#62;&#60;/span&#62;
    &#60;/a&#62;
  &#60;/li&#62;
&#60;/ul&#62;<hr>
&#60;ul class="paginacion"&#62;
  &#60;li class="paginacion__item"&#62;
    &#60;a href="#" class="paginacion__link"&#62;
      &#60;span class="icon-elem icon-elem--navigate_before"&#62;&#60;/span&#62;
    &#60;/a&#62;
  &#60;/li&#62;
  &#60;li class="paginacion__item"&#62;
    &#60;a href="#" class="paginacion__link"&#62;
      1
    &#60;/a&#62;
  &#60;/li&#62;
  &#60;li class="paginacion__item"&#62;
    &#60;a href="#" class="paginacion__link"&#62;
      2
    &#60;/a&#62;
  &#60;/li&#62;
  &#60;li class="paginacion__item"&#62;
    &#60;a href="#" class="paginacion__link"&#62;
      3
    &#60;/a&#62;
  &#60;/li&#62;
  &#60;li class="paginacion__item"&#62;
    &#60;a href="#" class="paginacion__link"&#62;
      4
    &#60;/a&#62;
  &#60;/li&#62;
  &#60;li class="paginacion__item paginacion__item--activo"&#62;
    &#60;a href="#" class="paginacion__link"&#62;
      5
    &#60;/a&#62;
  &#60;/li&#62;
  &#60;li class="paginacion__item paginacion__item--desactivado"&#62;
    &#60;a href="#" class="paginacion__link"&#62;
      &#60;span class="icon-elem icon-elem--navigate_next"&#62;&#60;/span&#62;
    &#60;/a&#62;
  &#60;/li&#62;
&#60;/ul&#62;</code></pre>
                <div class="code-box__button">
                  <button data-clipboard-target="#copy-paginacion" class="button code-box__copy" type="button"><span class="icon-elem icon-elem--content_copy"></span>Copiar</button>
                </div>
              </div>
            </div>
            <div id="paginacion-css">
              <div class="code-box">
                <div class="css-modificadores">
                  <h6>Modificadores de estilos</h6>
                  <ul class="listado-clases">
                    <li><code class="clases">paginacion__item--activo</code></li>
                    <li><code class="clases">paginacion__item--desactivado</code></li>
                  </ul>
                </div>
              </div>
            </div>
          </section>
        </div>
        <!--CODIGO-->
      </div>
      <div class="item">
        <div class="title-section">
          <h2>Men&#250; de navegaci&#243;n</h2>
          <div class="divider"></div>
        </div>
        <p>Este complemento facilita ordenar distintas secciones y componentes del sitio privado bajo par&#225;metros l&#243;gicos. Para su implementaci&#243;n previamente es necesario establecer cu&#225;les ser&#225;n las opciones principales.</p>
        <div class="tipogafria">
          <div class="agrupador-contenido">
            <nav class="navegacion navegacion--principal">
              <ul class="navegacion__menu">
                <li class="navegacion__item"><a class="navegacion__link" href="#">contenido 1</a></li>
                <li class="navegacion__item"><a class="navegacion__link" href="#">contenido 2</a></li>
                <li class="navegacion__item navegacion__item--activo"><a class="navegacion__link" href="#">contenido 3</a></li>
                <li class="navegacion__item"><a class="navegacion__link" href="#">contenido 4</a></li>
                <li class="navegacion__item"><a class="navegacion__link" href="#">contenido 5</a></li>
              </ul>
            </nav>
            <nav class="navegacion navegacion--secundaria">
              <ul class="navegacion__menu">
                <li class="navegacion__item navegacion__item--activo"><a class="navegacion__link" href="#">sub-contenido 1</a></li>
                <li class="navegacion__item"><a class="navegacion__link" href="#">sub-contenido 2</a></li>
                <li class="navegacion__item"><a class="navegacion__link" href="#">sub-contenido 3</a></li>
                <li class="navegacion__item"><a class="navegacion__link" href="#">sub-contenido 4</a></li>
                <li class="navegacion__item"><a class="navegacion__link" href="#">sub-contenido 5</a></li>
              </ul>
            </nav>
          </div>
        </div>
        <!--CODIGO-->
        <div class="wrapper">
          <ul class="tabs clearfix" data-tabgroup="breadcrumbs">
            <li><a href="#nav-html"><span class="texto-desplegable">html</span><span class="icon-elem icon-elem--arrow_drop_down"></span></a></li>
            <li><a href="#nav-css"><span class="texto-desplegable">css</span><span class="icon-elem icon-elem--arrow_drop_down"></span></a></li>
          </ul>
          <section id="paginacion" class="tabgroup close">
            <div id="nav-html">
              <div class="code-box">
                <pre id="copy-navegacion" class="prettyprint"><code class="lang-html">&#60;nav class="navegacion navegacion--principal"&#62;
  &#60;ul class="navegacion__menu"&#62;
    &#60;li class="navegacion__item"&#62;
      &#60;a href="#" class="navegacion__link"&#62;
        contenido 1
      &#60;/a&#62;
    &#60;/li&#62;
    &#60;li class="navegacion__item"&#62;
      &#60;a href="#" class="navegacion__link"&#62;
        contenido 2
      &#60;/a&#62;
    &#60;/li&#62;
    &#60;li class="navegacion__item"&#62;
      &#60;a href="#" class="navegacion__link navegacion__link--activo"&#62;
        contenido 3
      &#60;/a&#62;
    &#60;/li&#62;
    &#60;li class="navegacion__item"&#62;
      &#60;a href="#" class="navegacion__link"&#62;
        contenido 4
      &#60;/a&#62;
    &#60;/li&#62;
    &#60;li class="navegacion__item"&#62;
      &#60;a href="#" class="navegacion__link"&#62;
        contenido 5
      &#60;/a&#62;
    &#60;/li&#62;
  &#60;/ul&#62;
&#60;/nav&#62;
&#60;nav class="navegacion navegacion--secundaria"&#62;
  &#60;ul class="navegacion__menu"&#62;
    &#60;li class="navegacion__item"&#62;
      &#60;a href="#" class="navegacion__link"&#62;
        contenido 1
      &#60;/a&#62;
    &#60;/li&#62;
    &#60;li class="navegacion__item"&#62;
      &#60;a href="#" class="navegacion__link"&#62;
        contenido 2
      &#60;/a&#62;
    &#60;/li&#62;
    &#60;li class="navegacion__item"&#62;
      &#60;a href="#" class="navegacion__link navegacion__link--activo"&#62;
        contenido 3
      &#60;/a&#62;
    &#60;/li&#62;
    &#60;li class="navegacion__item"&#62;
      &#60;a href="#" class="navegacion__link"&#62;
        contenido 4
      &#60;/a&#62;
    &#60;/li&#62;
    &#60;li class="navegacion__item"&#62;
      &#60;a href="#" class="navegacion__link"&#62;
        contenido 5
      &#60;/a&#62;
    &#60;/li&#62;
  &#60;/ul&#62;
&#60;/nav&#62;</code></pre>
                <div class="code-box__button">
                  <button data-clipboard-target="#copy-navegacion" class="button code-box__copy" type="button"><span class="icon-elem icon-elem--content_copy"></span>Copiar</button>
                </div>
              </div>
            </div>
            <div id="nav-css">
              <div class="code-box">
                <div class="css-modificadores">
                  <h6>Modificadores de estilos</h6>
                  <ul class="listado-clases">
                    <li><code class="clases">navegacion__link--activo</code></li>
                  </ul>
                </div>
              </div>
            </div>
          </section>
        </div>
        <!--CODIGO-->
      </div>
    </section>
  </div>
  <?php include 'footer.php';?>
</main>
