$(document).ready(function () {

    $('#area-biografia').keyup(function () {
        var $textarea = $(this),
            maxlength = parseInt($textarea.attr('maxlength')),
            valuelength = $textarea.val().length;

        $('#max-biografia').text(maxlength - valuelength);
    });
    $('#area-presentacion').keyup(function () {
        var $textarea = $(this),
            maxlength = parseInt($textarea.attr('maxlength')),
            valuelength = $textarea.val().length;

        $('#max-presentacion').text(maxlength - valuelength);
    });

    var $textbuscar = 'Búsqueda';
    var $textcancelar = 'Cancelar';
    $('#buscar').click(function () {
        $('#search-box--desktop').slideToggle(200);
        $(this).parent().toggleClass('active');
        $('.icon-elem', this).toggleClass('icon-elem--search');
        $('.icon-elem', this).toggleClass('icon-elem--close');
        $('.text-btn', this).text(function (i, text) {
            return text === $textcancelar ? $textbuscar : $textcancelar;
        });
    });

    $('.list-dropdown__list').hide();
    $('.list-dropdown__trigger').click(function () {
        $('.list-dropdown__list').slideToggle();
        $('.icon-elem', this).toggleClass('animate');
        $(this).toggleClass('active');
    });

    //navegacion
    var menu = $('[data-role="nav-body"]');
    $('[data-role="nav-deployer"]').click(function(){
      $(menu).toggleClass('open-menu');
    });

    //hover scroll
    var target = $("#target");
    var targetTexto = $("#target h2").text();
    var destiny = $('.destiny');
    target.mouseenter(function(){
      $(destiny).addClass('overMouse');
    });
    target.mouseleave(function(){
      $(destiny).removeClass('overMouse');
    });

//Nav scrollTop
$(window).scroll(function(event){
  var scrollVal = $(document).scrollTop().valueOf();

  if(scrollVal > 96){
    $('.other-nav').addClass('nav-animation');
  }else{
    $('.other-nav').removeClass('nav-animation');
  }
});


$("#showmenu").click(function(e){
			$("#menu").toggleClass("show");
		});
		$("#menu a").click(function(event){
			if($(this).next('ul').length){
        event.preventDefault();
				$(this).next().toggle('slow');
				$(this).children('i:last-child').toggleClass('fa-caret-down fa-caret-left');
			}
		});

    $(".navegacion-lateral__item--sub a").click(function(event){
			if($(this).next('ul').length){
        event.preventDefault();
				$(this).next().toggle('slow');
				$(this).children('i:last-child').toggleClass('fa-caret-down fa-caret-left');
			}
		});

    //desplegables
    var click = $('[data-desplegable="click"]');
    var icono = $('[data-desplegable="icono"]');
    var desplegable = $('[data-desplegable="desplegable"]');
    $(click).click(function(){
      $(this).parent().next().slideToggle('open-desplegable');
      $(this).css({
      transform: 'rotate(90deg)'
      },1000);
    });



  //active menu
  $(".navegacion-lateral__item--child a").removeClass("sub-activo");
  var urlActual = window.location.pathname;
  var activePage = urlActual.substring(urlActual.lastIndexOf("/")+1);
  var count = 0;
  $(".navegacion-lateral__item--child a").each(function(){
    var currentPage = this.href.substring(this.href.lastIndexOf("/")+1);
    if(activePage == currentPage){
      $(this).addClass("sub-activo");
      count++;
    }
  });

  if(urlActual != '/'){
    $(".in").css({
      display : "block"
    })
  };

  //TABS
  //$('.close').css({
    //display : "none"
  //});
$('.tabgroup > div').hide();
//$('.tabgroup > div:first-of-type').show();
$('.tabs a').click(function(e){
e.preventDefault();
  var $this = $(this),
      tabgroup = '#'+$this.parents('.tabs').data('tabgroup'),
      others = $this.closest('li').siblings().children('a'),

      target = $this.attr('href');
      //$(target).parents().css({
        //display : "block"
      //});
  $this.addClass('active');
  $($this).children('div').hide();
  $(others).removeClass('active');
  $(others).addClass('des');
  ocultar = $(others).parents('li').parents('ul').siblings('section').children(target);
  hermano = ocultar.siblings();
  $(hermano).hide();
  $(target).slideToggle();
  $this.stop();
})

});
