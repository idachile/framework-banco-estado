<?php include 'header.php';?>
<main>
  <section class="content breadcrumbs-bar-frame">
    <?php include 'partials/breadcrumbs.php';?>
  </section>
  <section class="content">
  <div class="introduccion">
    <div class="title-section">
      <h1>Ejemplos aplicados</h1>
    </div>
    <p>Antes de poner a disposici&#243;n de los usuarios nuevos desarrollos o funciones optimizadas en el sitio privado de BancoEstado, es necesario revisar de forma interna que cumplan con los par&#225;metros establecidos por esta gu&#237;a de estilos digitales. Desde la distribuci&#243;n de los elementos hasta las respuestas de botones y formularios, deben pasar por diversos controles antes de su puesta en servicio.</p>
  </div>
</section>
  <div class="content-box">
    <section>
      <div class="item">
        <div class="title-section">
          <h2>Pago de servicios</h2>
          <div class="divider"></div>
        </div>
        <p>Es una operaci&#243;n com&#250;n en la banca digital. Asociar un pago a una cuenta bancaria, o pagar online un servicio, requiere componentes que faciliten al usuario la operaci&#243;n. A continuaci&#243;n, pueden revisar c&#243;mo aplicar los distintos elementos de esta gu&#237;a de estilos digitales.</p>
        <div class="tipogafria">
          <div class="agrupador-contenido">
            <a href="pago.html">Enlace a Ejemplo</a>
          </div>
        </div>
      </div>
    </section>
  </div>
<?php include 'footer.php';?>
</main>
