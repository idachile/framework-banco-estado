<?php include 'header.php';?>
<main>
  <section class="content breadcrumbs-bar-frame">
    <?php include 'partials/breadcrumbs.php';?>
  </section>
  <section class="content">
  <div class="introduccion">
    <div class="title-section">
      <h1 class="m-0">Iconos</h1>
    </div>
    <p>El uso del color en las distintas secciones y m&#243;dulos del sitio privado es fundamental. Permite mantener la identidad visual de BancoEstado y resaltar la ubicaci&#243;n de los elementos m&#225;s importantes de cada proceso. Otro de sus objetivos es guiar al usuario en toda operaci&#243;n que realice: por ejemplo, destacar el espacio destinado al ingreso de datos o mostrar el avance de una transacci&#243;n.</p>
  </div>
</section>
  <div class="content-box">
    <section>
      <div class="item">
        <div class="colores">
          <div class="gridle-row">
            <div class="gridle-gr-2 content-color">
              <div class="contenedor-iconos">
                <div class="icono-muestra">
                  <span class="icono icono--descarga"></span>
                </div>
                <div class="icono-clase">
                  <p>icono--descarga</p>
                </div>
              </div>
            </div>
            <div class="gridle-gr-2 content-color">
              <div class="contenedor-iconos">
                <div class="icono-muestra">
                  <span class="icono icono--pdf"></span>
                </div>
                <div class="icono-clase">
                  <p>icono--pdf</p>
                </div>
              </div>
            </div>
            <div class="gridle-gr-2 content-color">
              <div class="contenedor-iconos">
                <div class="icono-muestra">
                  <span class="icono icono--imprimir"></span>
                </div>
                <div class="icono-clase">
                  <p>icono--imprimir</p>
                </div>
              </div>
            </div>
            <div class="gridle-gr-2 content-color">
              <div class="contenedor-iconos">
                <div class="icono-muestra">
                  <span class="icono icono--mail"></span>
                </div>
                <div class="icono-clase">
                  <p>icono--mail</p>
                </div>
              </div>
            </div>
            <div class="gridle-gr-2 content-color">
              <div class="contenedor-iconos">
                <div class="icono-muestra">
                  <span class="icono icono--exito"></span>
                </div>
                <div class="icono-clase">
                  <p>icono--exito</p>
                </div>
              </div>
            </div>
            <div class="gridle-gr-2 content-color">
              <div class="contenedor-iconos">
                <div class="icono-muestra">
                  <span class="icono icono--informacion"></span>
                </div>
                <div class="icono-clase">
                  <p>icono--informacion</p>
                </div>
              </div>
            </div>
            <div class="gridle-gr-2 content-color">
              <div class="contenedor-iconos">
                <div class="icono-muestra">
                  <span class="icono icono--error"></span>
                </div>
                <div class="icono-clase">
                  <p>icono--error</p>
                </div>
              </div>
            </div>
            <div class="gridle-gr-2 content-color">
              <div class="contenedor-iconos">
                <div class="icono-muestra">
                  <span class="icono icono--tooltip"></span>
                </div>
                <div class="icono-clase">
                  <p>icono--tooltip</p>
                </div>
              </div>
            </div>
            <div class="gridle-gr-2 content-color">
              <div class="contenedor-iconos">
                <div class="icono-muestra">
                  <span class="icono icono--calendario"></span>
                </div>
                <div class="icono-clase">
                  <p>icono--calendario</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
<?php include 'footer.php';?>
</main>
