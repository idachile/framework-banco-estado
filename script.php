<script src="scripts/libs/jquery-3.1.0.min.js"></script>
<!--ESQUELETO-->
<script src="scripts/min/main.min.js"></script>
<script src="scripts/main.js"></script>
<script src="scripts/src/plugins/prettify.js"></script>
<!--<script src="scripts/libs/clipboard.min.js"></script>-->
<script src="scripts/libs/clip.js"></script>
<!--ESQUELETO-->
<!--FRAMEWORK-->
<script src="scripts/be/main.min.js"></script>
  <script type="text/javascript">
  //tablesorter
    $(document).ready(function(){$("#id_tabla").tablesorter();});
  //inicializacion clipboard
    new Clipboard('.code-box__copy');
  //tooltip clipboard
    button = $('.code-box__copy');
    mensaje = '<p class="mensaje-flotante">Copiado!</p>';
    button.click(function(){
      button.append(mensaje);
      $('.mensaje-flotante').fadeOut(2000);
    });
  </script>
<!--FRAMEWORK-->
