<?php include 'header.php';?>
<main>
  <section class="content breadcrumbs-bar-frame">
    <?php include 'partials/breadcrumbs.php';?>
  </section>
  <section class="content">
  <div class="introduccion">
    <div class="title-section">
      <h1>Alertas</h1>
    </div>
    <p>El sitio privado de BancoEstado permite la realizaci&#243;n de una amplia cantidad de procesos que requieren acciones espec&#237;ficas. Una de las formas para indicarle al usuario el resultado de la acci&#243;n, ya sea positivo o negativo, es usando las alertas. Entre sus caracter&#237;sticas resalta que su mensaje debe ser claro y diferenciarse del resto de los elementos del wireframe.</p>
  </div>
</section>
  <div class="content-box">
    <section>
      <div class="item">
        <div class="title-section">
          <h2>Mensajes de alertas</h2>
          <div class="divider"></div>
        </div>
        <p>Durante la utilizaci&#243;n de la plataforma es posible realizar diversas operaciones, las cuales pueden ser positivas o negativas. Si el proceso acaba exitosamente, ocurre un error o se quiere resaltar un hecho particular, cada alerta tiene un formato espec&#237;fico.</p>
        <div class="tipogafria">
          <div class="agrupador-contenido">
            <div class="caja-alerta caja-alerta--exito">
              <div class="caja-alerta__icono">
                <span class="icono icono--exito"></span>
              </div>
              <div class="caja-alerta__cuerpo">
                <h6 class="caja-alerta__titulo">Este es un mensaje de &#233;xito</h6>
                <div class="caja-alerta__mensaje">
                  <p class="caja-alerta__texto">Se ha enviado un comprobante al correo <strong>eduardo.r&#64;gmail.com</strong></p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!--CODIGO-->
        <div class="wrapper">
          <ul class="tabs clearfix" data-tabgroup="mensajes">
            <li><a href="#mensaje-exito"><span class="texto-desplegable">html</span><span class="icon-elem icon-elem--arrow_drop_down"></span></a></li>
            <li><a href="#mensaje-exito-css"><span class="texto-desplegable">css</span><span class="icon-elem icon-elem--arrow_drop_down"></span></a></li>
          </ul>
          <section id="btn-secundario" class="tabgroup close">
            <div id="mensaje-exito">
              <div class="code-box">
                <div class="code-box__button">
                  <button class="btn button code-box__copy" type="button" onclick="copyToClipboard('#cp-alrt-ex')"><span class="icon-elem icon-elem--content_copy"></span>Copiar</button>
                </div>
                  <pre id="cp-alrt-ex" class="prettyprint linenums"><code class="lang-html">&#60;div class="caja-alerta caja-alerta--exito"&#62;
  &#60;div class="caja-alerta__icono"&#62;
    &#60;span class="icono icono--exito"&#62;&#60;/span&#62;
  &#60;/div&#62;
  &#60;div class="caja-alerta__cuerpo"&#62;
    &#60;h6 class="caja-alerta__titulo"&#62;Este es un mensaje de &#233;xito&#60;/p&#62;
    &#60;div class="caja-alerta__mensaje"&#62;
      &#60;p class="contenedor__texto"&#62;Se ha enviado un comprobante al correo &#225;&#60;strong&#62;eduardo.r@gmail.com&#60;/strong&#62;&#60;/p&#62;
    &#60;/div&#62;
  &#60;/div&#62;
&#60;/div&#62;</code></pre>
              </div>
            </div>
            <div id="mensaje-exito-css">
              <div class="code-box">
                <div class="css-modificadores">
                  <h6>Modificadores de estilos</h6>
                  <ul class="listado-clases">
                    <li><code class="clases">caja-alerta--exito</code></li>
                  </ul>
                </div>
              </div>
            </div>
          </section>
        </div>
        <!--CODIGO-->
        <div class="tipogafria">
          <div class="agrupador-contenido">
            <div class="caja-alerta caja-alerta--informacion">
              <div class="caja-alerta__icono">
                <span class="icono icono--informacion"></span>
              </div>
              <div class="caja-alerta__cuerpo">
                <h6 class="caja-alerta__titulo">Este es un mensaje de informaci&#243;n</h6>
                <div class="caja-alerta__mensaje">
                  <p>Se ha enviado un comprobante al correo <span class="caja-alerta__correo">eduardo.r&#64;gmail.com</span></p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!--CODIGO-->
        <div class="wrapper">
          <ul class="tabs clearfix" data-tabgroup="btn-secundario">
            <li><a href="#mensaje-info"><span class="texto-desplegable">html</span><span class="icon-elem icon-elem--arrow_drop_down"></span></a></li>
            <li><a href="#mensaje-info-css"><span class="texto-desplegable">css</span><span class="icon-elem icon-elem--arrow_drop_down"></span></a></li>
          </ul>
          <section id="btn-secundario" class="tabgroup close">
            <div id="mensaje-info">
              <div class="code-box">
                <div class="code-box__button">
                  <button class="button code-box__copy" type="button" onclick="copyToClipboard('#cp-alrt-inf')"><span class="icon-elem icon-elem--content_copy"></span>Copiar</button>
                </div>
                <pre id="cp-alrt-inf" class="prettyprint linenums"><code class="lang-html">&#60;div class="caja-alerta caja-alerta--informacion"&#62;
  &#60;div class="caja-alerta__icono"&#62;
    &#60;span class="icono icono--informacion"&#62;&#60;/span&#62;
  &#60;/div&#62;
  &#60;div class="caja-alerta__cuerpo"&#62;
    &#60;h6 class="caja-alerta__titulo"&#62;Este es un mensaje de informacion&#60;/p&#62;
    &#60;div class="caja-alerta__mensaje"&#62;
      &#60;p class="contenedor__texto"&#62;Se ha enviado un comprobante al correo &#225;&#60;strong&#62;eduardo.r@gmail.com&#60;/strong&#62;&#60;/p&#62;
    &#60;/div&#62;
  &#60;/div&#62;
&#60;/div&#62;</code></pre>
              </div>
            </div>
            <div id="mensaje-info-css">
              <div class="code-box">
                <div class="css-modificadores">
                  <h6>Modificadores de estilos</h6>
                  <ul class="listado-clases">
                    <li><code class="clases">caja-alerta--informacion</code></li>
                  </ul>
                </div>
              </div>
            </div>
          </section>
        </div>
        <!--CODIGO-->
        <div class="tipogafria">
          <div class="agrupador-contenido">
            <div class="caja-alerta caja-alerta--error">
              <div class="caja-alerta__icono">
                <span class="icono icono--error"></span>
              </div>
              <div class="caja-alerta__cuerpo">
                <h6 class="caja-alerta__titulo">Este es un mensaje de error</h6>
                <div class="caja-alerta__mensaje">
                  <p>Se ha enviado un comprobante al correo <span class="caja-alerta__correo">eduardo.r&#64;gmail.com</span></p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!--CODIGO-->
        <div class="wrapper">
          <ul class="tabs clearfix" data-tabgroup="btn-secundario">
            <li><a href="#mensaje-error"><span class="texto-desplegable">html</span><span class="icon-elem icon-elem--arrow_drop_down"></span></a></li>
            <li><a href="#mensaje-error-css"><span class="texto-desplegable">css</span><span class="icon-elem icon-elem--arrow_drop_down"></span></a></li>
          </ul>
          <section id="btn-secundario" class="tabgroup close">
            <div id="mensaje-error">
              <div class="code-box">
                <div class="code-box__button">
                  <button id="buttonCopy" class="button code-box__copy" type="button" onclick="copyToClipboard('#cp-alrt-err')"><span class="icon-elem icon-elem--content_copy"></span>Copiar</button>
                </div>
                <pre id="cp-alrt-err" class="prettyprint linenums"><code class="lang-html">&#60;div class="caja-alerta caja-alerta--error"&#62;
  &#60;div class="caja-alerta__icono"&#62;
    &#60;span class="icono icono--error"&#62;&#60;/span&#62;
  &#60;/div&#62;
  &#60;div class="caja-alerta__cuerpo"&#62;
    &#60;h6 class="caja-alerta__titulo"&#62;Este es un mensaje de &#233;xito&#60;/p&#62;
    &#60;div class="caja-alerta__mensaje"&#62;
      &#60;p class="caja-alerta__texto"&#62;Se ha enviado un comprobante al correo &#225;&#60;strong&#62;eduardo.r@gmail.com&#60;/strong&#62;&#60;/p&#62;
    &#60;/div&#62;
  &#60;/div&#62;
&#60;/div&#62;</code></pre>
              </div>
            </div>
            <div id="mensaje-error-css">
              <div class="code-box">
                <div class="css-modificadores">
                  <h6>Modificadores de estilos</h6>
                  <ul class="listado-clases">
                    <li><code class="clases">caja-alerta--error</code></li>
                  </ul>
                </div>
              </div>
            </div>
          </section>
        </div>
        <!--CODIGO-->
      </div>
    </section>
  </div>
<?php include 'footer.php';?>
</main>
