<?php include 'header.php';?>
<main>
  <section class="content breadcrumbs-bar-frame">
    <?php include 'partials/breadcrumbs.php';?>
  </section>
  <section class="content">
  <div class="introduccion">
    <div class="title-section">
      <h1>Tablas</h1>
    </div>
    <p>Las tablas son uno de los elementos comunes cuando se necesita desplegar informaci&#243;n de forma ordenada. Su diseño permite que tanto texto y elementos anexos (botones, campos de texto y otros) puedan compartir espacio dentro del sitio privado. Su implementaci&#243;n debe responder a las necesidades puntuales del proceso que se ejecutar&#225; y de los resultados que exhibir&#225;.</p>
  </div>
</section>
  <div class="content-box">
    <section>
      <div class="item">
        <div class="title-section">
          <h2>Tabla B&#225;sica</h2>
          <div class="divider"></div>
        </div>
        <p>Si &#250;nicamente es necesario desplegar informaci&#243;n, y permitir operaciones b&#225;sicas como ordenarlas, este formato es el correcto. El texto que contenga, como los otros elementos del wireframe, deben responder a las pautas de esta gu&#237;a de estilos digitales.</p>
        <div class="tipogafria">
          <div class="agrupador-contenido">
            <table class="tabla">
              <thead class="tabla__head">
                <tr class="tabla__fila">
                  <th class="tabla__titulo"><span class="icon-elem icon-elem--arrow_drop_down"></span>Columna 1</th>
                  <th class="tabla__titulo"><span class="icon-elem icon-elem--arrow_drop_down"></span>Columna 2</th>
                  <th class="tabla__titulo"><span class="icon-elem icon-elem--arrow_drop_down"></span>Columna 3</th>
                  <th class="tabla__titulo"><span class="icon-elem icon-elem--arrow_drop_down"></span>Columna 4</th>
                </tr>
              </thead>
              <tbody class="tabla__body">
                <tr class="tabla__fila">
                  <td class="tabla__celda">Smith</td>
                  <td class="tabla__celda">Jhon</td>
                  <td class="tabla__celda">jhonsmit@gmail.com</td>
                  <td class="tabla__celda">$50.000</td>
                </tr>
                <tr class="tabla__fila">
                  <td class="tabla__celda">Celda 5</td>
                  <td class="tabla__celda">Celda 6</td>
                  <td class="tabla__celda">Celda 7</td>
                  <td class="tabla__celda">Celda 8</td>
                </tr>
                <tr class="tabla__fila">
                  <td class="tabla__celda">Celda 9</td>
                  <td class="tabla__celda">Celda 10</td>
                  <td class="tabla__celda">Celda 11</td>
                  <td class="tabla__celda">Celda 12</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
        <!--CODIGO-->
        <div class="wrapper">
          <ul class="tabs clearfix" data-tabgroup="tablas">
            <li><a href="#tabla-html"><span class="texto-desplegable">html</span><span class="icon-elem icon-elem--arrow_drop_down"></span></a></li>
          </ul>
          <section id="tablas" class="tabgroup close">
            <div id="tabla-html">
              <div class="code-box">
                <div class="code-box__button">
                  <button class="button code-box__copy" type="button" onclick="copyToClipboard('#copy-tabla-1')"><span class="icon-elem icon-elem--content_copy"></span>Copiar</button>
                </div>
                  <pre id="copy-tabla-1" class="prettyprint linenums"><code class="lang-html">&#60;table id="id_tabla" class="tabla tablesorter"&#62;
  &#60;thead class="tabla__head"&#62;
    &#60;tr class="tabla__fila"&#62;
      &#60;th class="tabla__titulo"&#62;
        &#60;span class="icon-elem icon-elem--arrow_drop_down"&#62;&#60;/span&#62;Columna 1
      &#60;/th&#62;
      &#60;th class="tabla__titulo"&#62;
        &#60;span class="icon-elem icon-elem--arrow_drop_down"&#62;&#60;/span&#62;Columna 2
      &#60;/th&#62;
      &#60;th class="tabla__titulo"&#62;
        &#60;span class="icon-elem icon-elem--arrow_drop_down"&#62;&#60;/span&#62;Columna 3
      &#60;/th&#62;
      &#60;th class="tabla__titulo"&#62;
        &#60;span class="icon-elem icon-elem--arrow_drop_down"&#62;&#60;/span&#62;Columna 4
      &#60;/th&#62;
    &#60;/tr&#62;
  &#60;/thead&#62;
  &#60;tbody class="tabla__body"&#62;
    &#60;tr class="tabla__fila"&#62;
      &#60;td class="tabla__celda"&#62;
        Smith
      &#60;/td&#62;
      &#60;td class="tabla__celda"&#62;
        Jhon
      &#60;/td&#62;
      &#60;td class="tabla__celda"&#62;
        jhonsmith@gmail.com
      &#60;/td&#62;
      &#60;td class="tabla__celda"&#62;
        $50.000
      &#60;/td&#62;
    &#60;/tr&#62;
    &#60;tr class="tabla__fila"&#62;
      &#60;td class="tabla__celda"&#62;
        Smith
      &#60;/td&#62;
      &#60;td class="tabla__celda"&#62;
        Jhon
      &#60;/td&#62;
      &#60;td class="tabla__celda"&#62;
        jhonsmith@gmail.com
      &#60;/td&#62;
      &#60;td class="tabla__celda"&#62;
        $50.000
      &#60;/td&#62;
    &#60;/tr&#62;
    &#60;tr class="tabla__fila"&#62;
      &#60;td class="tabla__celda"&#62;
        Smith
      &#60;/td&#62;
      &#60;td class="tabla__celda"&#62;
        Jhon
      &#60;/td&#62;
      &#60;td class="tabla__celda"&#62;
        jhonsmith@gmail.com
      &#60;/td&#62;
      &#60;td class="tabla__celda"&#62;
        $50.000
      &#60;/td&#62;
    &#60;/tr&#62;
  &#60;/tbody&#62;
&#60;/table&#62;</code></pre>
              </div>
            </div>
          </section>
        </div>
        <!--CODIGO-->
      </div>
      <div class="item">
        <div class="title-section">
          <h2>Tabla con radio button</h2>
          <div class="divider"></div>
        </div>
        <p>La principal diferencia con el formato anterior radica en la incorporaci&#243;n de un elemento de selecci&#243;n. Le permite al usuario elegir al menos un dato para su posterior procesamiento. En su construcci&#243;n los colores deben respetar la paleta establecida previamente.</p>
        <div class="tipogafria">
            <div class="agrupador-contenido">
              <table class="tabla">
                <thead class="tabla__head">
                  <tr class="tabla__fila">
                    <th class="tabla__titulo"><span class="icon-elem icon-elem--arrow_drop_down"></span>Columna 1</th>
                    <th class="tabla__titulo"><span class="icon-elem icon-elem--arrow_drop_down"></span>Columna 2</th>
                    <th class="tabla__titulo"><span class="icon-elem icon-elem--arrow_drop_down"></span>Columna 3</th>
                    <th class="tabla__titulo"><span class="icon-elem icon-elem--arrow_drop_down"></span>Columna 4</th>
                  </tr>
                </thead>
                <tbody class="tabla__body">
                  <tr class="tabla__fila">
                    <td class="tabla__celda">
                      <label class="formulario__input">
                        <input type="radio" value="1">
                        <span class="formulario__input--radio"></span>
                        <label class="formulario__label"></label>
                      </label>Celda 1
                    </td>
                    <td class="tabla__celda">Celda 2</td>
                    <td class="tabla__celda">Celda 3</td>
                    <td class="tabla__celda">Celda 4</td>
                  </tr>
                  <tr class="tabla__fila">
                    <td class="tabla__celda">
                      <label class="formulario__input">
                        <input type="radio" value="2">
                        <span class="formulario__input--radio"></span>
                        <label class="formulario__label"></label>
                      </label>Celda 5
                    </td>
                    <td class="tabla__celda">Celda 6</td>
                    <td class="tabla__celda">Celda 7</td>
                    <td class="tabla__celda">Celda 8</td>
                  </tr>
                  <tr class="tabla__fila">
                    <td class="tabla__celda">
                      <form class="formulario" action="index.html" method="post">
                        <label class="formulario__input">
                          <input type="radio" name="radio" value="radio">
                            <span class="formulario__input--radio"></span>
                          <label class="formulario__label">Campo 1</label>
                        </label>
                      </form>
                    </td>
                    <td class="tabla__celda">Celda 10</td>
                    <td class="tabla__celda">Celda 11</td>
                    <td class="tabla__celda">Celda 12</td>
                  </tr>
                </tbody>
              </table>
            </div>
        </div>
        <!--CODIGO-->
        <div class="wrapper">
          <ul class="tabs clearfix" data-tabgroup="tablas">
            <li><a href="#tabla-radio-html"><span class="texto-desplegable">html</span><span class="icon-elem icon-elem--arrow_drop_down"></span></a></li>
          </ul>
          <section id="tablas" class="tabgroup close">
            <div id="tabla-radio-html">
              <div class="code-box">
                <div class="code-box__button">
                  <button class="button code-box__copy" type="button" onclick="copyToClipboard('#copy-tabla-2')"><span class="icon-elem icon-elem--content_copy"></span>Copiar</button>
                </div>
                  <pre id="copy-tabla-2" class="prettyprint linenums"><code class="lang-html">&#60;table id="id_tabla" class="tabla tablesorter"&#62;
  &#60;thead class="tabla__head"&#62;
    &#60;tr class="tabla__fila"&#62;
      &#60;th class="tabla__titulo"&#62;
        &#60;span class="icon-elem icon-elem--arrow_drop_down"&#62;&#60;/span&#62;Columna 1
      &#60;/th&#62;
      &#60;th class="tabla__titulo"&#62;
        &#60;span class="icon-elem icon-elem--arrow_drop_down"&#62;&#60;/span&#62;Columna 2
      &#60;/th&#62;
      &#60;th class="tabla__titulo"&#62;
        &#60;span class="icon-elem icon-elem--arrow_drop_down"&#62;&#60;/span&#62;Columna 3
      &#60;/th&#62;
      &#60;th class="tabla__titulo"&#62;
        &#60;span class="icon-elem icon-elem--arrow_drop_down"&#62;&#60;/span&#62;Columna 4
      &#60;/th&#62;
    &#60;/tr&#62;
  &#60;/thead&#62;
  &#60;tbody class="tabla__body"&#62;
    &#60;tr class="tabla__fila"&#62;
      &#60;td class="tabla__celda"&#62;
        Smith
      &#60;/td&#62;
      &#60;td class="tabla__celda"&#62;
        Jhon
      &#60;/td&#62;
      &#60;td class="tabla__celda"&#62;
        jhonsmith@gmail.com
      &#60;/td&#62;
      &#60;td class="tabla__celda"&#62;
        $50.000
      &#60;/td&#62;
    &#60;/tr&#62;
    &#60;tr class="tabla__fila"&#62;
      &#60;td class="tabla__celda"&#62;
        Smith
      &#60;/td&#62;
      &#60;td class="tabla__celda"&#62;
        Jhon
      &#60;/td&#62;
      &#60;td class="tabla__celda"&#62;
        jhonsmith@gmail.com
      &#60;/td&#62;
      &#60;td class="tabla__celda"&#62;
        $50.000
      &#60;/td&#62;
    &#60;/tr&#62;
    &#60;tr class="tabla__fila"&#62;
      &#60;td class="tabla__celda"&#62;
        Smith
      &#60;/td&#62;
      &#60;td class="tabla__celda"&#62;
        Jhon
      &#60;/td&#62;
      &#60;td class="tabla__celda"&#62;
        jhonsmith@gmail.com
      &#60;/td&#62;
      &#60;td class="tabla__celda"&#62;
        $50.000
      &#60;/td&#62;
    &#60;/tr&#62;
  &#60;/tbody&#62;
&#60;/table&#62;</code></pre>
              </div>
            </div>
          </section>
        </div>
        <!--CODIGO-->
      </div>
      <div class="item">
        <div class="title-section">
          <h2>Tabla con botones</h2>
          <div class="divider"></div>
        </div>
        <p>Implementar este tipo de soluciones tiene como contexto realizar una acci&#243;n espec&#237;fica con la informaci&#243;n desplegada. La existencia de un bot&#243;n le facilita al usuario el uso de los datos entregados, definiendo cu&#225;l de ellos ser&#225; usado en un proceso posterior.</p>
        <div class="tipogafria">
          <div class="agrupador-contenido">
            <table class="tabla">
              <thead class="tabla__head">
                <tr class="tabla__fila">
                  <th class="tabla__titulo"><span class="icon-elem icon-elem--arrow_drop_down"></span>Columna 1</th>
                  <th class="tabla__titulo"><span class="icon-elem icon-elem--arrow_drop_down"></span>Columna 2</th>
                  <th class="tabla__titulo"><span class="icon-elem icon-elem--arrow_drop_down"></span>Columna 3</th>
                  <th class="tabla__titulo"><span class="icon-elem icon-elem--arrow_drop_down"></span>Columna 4</th>
                  <th class="tabla__titulo">&#32;</th>
                </tr>
              </thead>
              <tbody class="tabla__body">
                <tr class="tabla__fila">
                  <td class="tabla__celda">Celda 1</td>
                  <td class="tabla__celda">Celda 2</td>
                  <td class="tabla__celda">Celda 3</td>
                  <td class="tabla__celda">Celda 4</td>
                  <td class="tabla__celda"><button class="btn btn--principal btn--borde" type="button" name="button">Pagar</button></td>
                </tr>
                <tr class="tabla__fila">
                  <td class="tabla__celda">Celda 5</td>
                  <td class="tabla__celda">Celda 6</td>
                  <td class="tabla__celda">Celda 7</td>
                  <td class="tabla__celda">Celda 8</td>
                  <td class="tabla__celda"><button class="btn btn--principal btn--borde" type="button" name="button">Pagar</button></td>
                </tr>
                <tr class="tabla__fila">
                  <td class="tabla__celda">Celda 9</td>
                  <td class="tabla__celda">Celda 10</td>
                  <td class="tabla__celda">Celda 11</td>
                  <td class="tabla__celda">Celda 12</td>
                  <td class="tabla__celda"><button class="btn btn--principal btn--borde" type="button" name="button">Pagar</button></td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
        <!--CODIGO-->
        <div class="wrapper">
          <ul class="tabs clearfix" data-tabgroup="tablas">
            <li><a href="#tabla-boton-html"><span class="texto-desplegable">html</span><span class="icon-elem icon-elem--arrow_drop_down"></span></a></li>
          </ul>
          <section id="tablas" class="tabgroup close">
            <div id="tabla-boton-html">
              <div class="code-box">
                <div class="code-box__button">
                  <button class="button code-box__copy" type="button" onclick="copyToClipboard('#copy-tabla-3')"><span class="icon-elem icon-elem--content_copy"></span>Copiar</button>
                </div>
                  <pre id="copy-tabla-3" class="prettyprint linenums"><code class="lang-html">&#60;table id="id_tabla" class="tabla tablesorter"&#62;
  &#60;thead class="tabla__head"&#62;
    &#60;tr class="tabla__fila"&#62;
      &#60;th class="tabla__titulo"&#62;
        &#60;span class="icon-elem icon-elem--arrow_drop_down"&#62;&#60;/span&#62;Columna 1
      &#60;/th&#62;
      &#60;th class="tabla__titulo"&#62;
        &#60;span class="icon-elem icon-elem--arrow_drop_down"&#62;&#60;/span&#62;Columna 2
      &#60;/th&#62;
      &#60;th class="tabla__titulo"&#62;
        &#60;span class="icon-elem icon-elem--arrow_drop_down"&#62;&#60;/span&#62;Columna 3
      &#60;/th&#62;
      &#60;th class="tabla__titulo"&#62;
        &#60;span class="icon-elem icon-elem--arrow_drop_down"&#62;&#60;/span&#62;Columna 4
      &#60;/th&#62;
      &#60;th class="tabla__titulo"&#62;
        &#32;
      &#60;/th&#62;
    &#60;/tr&#62;
  &#60;/thead&#62;
  &#60;tbody class="tabla__body"&#62;
    &#60;tr class="tabla__fila"&#62;
      &#60;td class="tabla__celda"&#62;
        Smith
      &#60;/td&#62;
      &#60;td class="tabla__celda"&#62;
        Jhon
      &#60;/td&#62;
      &#60;td class="tabla__celda"&#62;
        jhonsmith@gmail.com
      &#60;/td&#62;
      &#60;td class="tabla__celda"&#62;
        $50.000
      &#60;/td&#62;
      &#60;td class="tabla__celda"&#62;
        &#60;button class="btn btn--borde" type="button" name="button"&#62;Pagar&#60;/button&#62;
      &#60;/td&#62;
    &#60;/tr&#62;
    &#60;tr class="tabla__fila"&#62;
      &#60;td class="tabla__celda"&#62;
        Smith
      &#60;/td&#62;
      &#60;td class="tabla__celda"&#62;
        Jhon
      &#60;/td&#62;
      &#60;td class="tabla__celda"&#62;
        jhonsmith@gmail.com
      &#60;/td&#62;
      &#60;td class="tabla__celda"&#62;
        $50.000
      &#60;/td&#62;
      &#60;td class="tabla__celda"&#62;
        &#60;button class="btn btn--borde" type="button" name="button"&#62;Pagar&#60;/button&#62;
      &#60;/td&#62;
    &#60;/tr&#62;
    &#60;tr class="tabla__fila"&#62;
      &#60;td class="tabla__celda"&#62;
        Smith
      &#60;/td&#62;
      &#60;td class="tabla__celda"&#62;
        Jhon
      &#60;/td&#62;
      &#60;td class="tabla__celda"&#62;
        jhonsmith@gmail.com
      &#60;/td&#62;
      &#60;td class="tabla__celda"&#62;
        $50.000
      &#60;/td&#62;
      &#60;td class="tabla__celda"&#62;
        &#60;button class="btn btn--borde" type="button" name="button"&#62;Pagar&#60;/button&#62;
      &#60;/td&#62;
    &#60;/tr&#62;
  &#60;/tbody&#62;
&#60;/table&#62;</code></pre>
              </div>
            </div>
          </section>
        </div>
        <!--CODIGO-->
      </div>
      <div class="item">
        <div class="title-section">
          <h2>Tabla con Ordenamiento</h2>
          <div class="divider"></div>
        </div>
        <p>Si &#250;nicamente es necesario desplegar informaci&#243;n, y permitir operaciones b&#225;sicas como ordenarlas, este formato es el correcto. El texto que contenga, como los otros elementos del wireframe, deben responder a las pautas de esta gu&#237;a de estilos digitales.</p>
        <div class="tipogafria">
          <div class="agrupador-contenido">
            <table id="id_tabla" class="tabla tablesorter">
              <thead class="tabla__head">
                <tr class="tabla__fila">
                  <th class="tabla__titulo"><span class="icon-elem icon-elem--arrow_drop_down"></span>Columna 1</th>
                  <th class="tabla__titulo"><span class="icon-elem icon-elem--arrow_drop_down"></span>Columna 2</th>
                  <th class="tabla__titulo"><span class="icon-elem icon-elem--arrow_drop_down"></span>Columna 3</th>
                  <th class="tabla__titulo"><span class="icon-elem icon-elem--arrow_drop_down"></span>Columna 4</th>
                </tr>
              </thead>
              <tbody class="tabla__body">
                <tr class="tabla__fila">
                  <td class="tabla__celda">Smith</td>
                  <td class="tabla__celda">Jhon</td>
                  <td class="tabla__celda">jhonsmit@gmail.com</td>
                  <td class="tabla__celda">$50.000</td>
                </tr>
                <tr class="tabla__fila">
                  <td class="tabla__celda">Celda 5</td>
                  <td class="tabla__celda">Celda 6</td>
                  <td class="tabla__celda">Celda 7</td>
                  <td class="tabla__celda">Celda 8</td>
                </tr>
                <tr class="tabla__fila">
                  <td class="tabla__celda">Celda 9</td>
                  <td class="tabla__celda">Celda 10</td>
                  <td class="tabla__celda">Celda 11</td>
                  <td class="tabla__celda">Celda 12</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
        <!--CODIGO-->
        <div class="wrapper">
          <ul class="tabs clearfix" data-tabgroup="tablas">
            <li><a href="#tabla-html"><span class="texto-desplegable">html</span><span class="icon-elem icon-elem--arrow_drop_down"></span></a></li>
            <li><a href="#tabla-js"><span class="texto-desplegable">js</span><span class="icon-elem icon-elem--arrow_drop_down"></span></a></li>
          </ul>
          <section id="tablas" class="tabgroup close">
            <div id="tabla-html">
              <div class="code-box">
                <div class="code-box__button">
                  <button class="button code-box__copy" type="button" onclick="copyToClipboard('#copy-tabla-4')"><span class="icon-elem icon-elem--content_copy"></span>Copiar</button>
                </div>
                  <pre id="copy-tabla-4" class="prettyprint linenums"><code class="lang-html">&#60;table id="id_tabla" class="tabla tablesorter"&#62;
  &#60;thead class="tabla__head"&#62;
    &#60;tr class="tabla__fila"&#62;
      &#60;th class="tabla__titulo"&#62;
        &#60;span class="icon-elem icon-elem--arrow_drop_down"&#62;&#60;/span&#62;Columna 1
      &#60;/th&#62;
      &#60;th class="tabla__titulo"&#62;
        &#60;span class="icon-elem icon-elem--arrow_drop_down"&#62;&#60;/span&#62;Columna 2
      &#60;/th&#62;
      &#60;th class="tabla__titulo"&#62;
        &#60;span class="icon-elem icon-elem--arrow_drop_down"&#62;&#60;/span&#62;Columna 3
      &#60;/th&#62;
      &#60;th class="tabla__titulo"&#62;
        &#60;span class="icon-elem icon-elem--arrow_drop_down"&#62;&#60;/span&#62;Columna 4
      &#60;/th&#62;
    &#60;/tr&#62;
  &#60;/thead&#62;
  &#60;tbody class="tabla__body"&#62;
    &#60;tr class="tabla__fila"&#62;
      &#60;td class="tabla__celda"&#62;
        Smith
      &#60;/td&#62;
      &#60;td class="tabla__celda"&#62;
        Jhon
      &#60;/td&#62;
      &#60;td class="tabla__celda"&#62;
        jhonsmith@gmail.com
      &#60;/td&#62;
      &#60;td class="tabla__celda"&#62;
        $50.000
      &#60;/td&#62;
    &#60;/tr&#62;
    &#60;tr class="tabla__fila"&#62;
      &#60;td class="tabla__celda"&#62;
        Smith
      &#60;/td&#62;
      &#60;td class="tabla__celda"&#62;
        Jhon
      &#60;/td&#62;
      &#60;td class="tabla__celda"&#62;
        jhonsmith@gmail.com
      &#60;/td&#62;
      &#60;td class="tabla__celda"&#62;
        $50.000
      &#60;/td&#62;
    &#60;/tr&#62;
    &#60;tr class="tabla__fila"&#62;
      &#60;td class="tabla__celda"&#62;
        Smith
      &#60;/td&#62;
      &#60;td class="tabla__celda"&#62;
        Jhon
      &#60;/td&#62;
      &#60;td class="tabla__celda"&#62;
        jhonsmith@gmail.com
      &#60;/td&#62;
      &#60;td class="tabla__celda"&#62;
        $50.000
      &#60;/td&#62;
    &#60;/tr&#62;
  &#60;/tbody&#62;
&#60;/table&#62;</code></pre>
              </div>
            </div>
            <div id="tabla-js">
              <div class="code-box">
                <div class="code-box__button">
                  <button id="buttonCopy" class="button code-box__copy" type="button" onclick="copyToClipboard('#copy-js-tabla')"><span class="icon-elem icon-elem--content_copy"></span>Copiar</button>
                </div>
                  <pre id="copy-js-tabla" class="prettyprint linenums"><code class="lang-js">$(document).ready(function()
 {
   $("#id_tabla").tablesorter();
 });</code></pre>
              </div>
            </div>
          </section>
        </div>
        <!--CODIGO-->
      </div>
    </section>
  </div>
<?php include 'footer.php';?>
</main>
