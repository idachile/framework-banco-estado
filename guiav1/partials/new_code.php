<!--CODIGO-->
<div class="wrapper">
  <ul class="tabs clearfix" data-tabgroup="btn-basico">
    <li><a href="#tab1" class="active">html</a></li>
    <li><a href="#tab2">css</a></li>
  </ul>
  <section id="btn-basico" class="tabgroup close">
    <div id="tab1">
      <div class="code-box">
        <div class="code-box__button">
          <button id="buttonCopy" class="button code-box__copy" type="button" data-func="copiar"><span class="icon-elem icon-elem--content_copy"></span>Copiar</button>
        </div>
        <pre class="prettyprint linenums">
          <code class="lang-html">
&#60;button class="btn btn--principal" type="button" name="button"&#62;Primary button&#60;/button&#62;
          </code>
        </pre>
      </div>
    </div>
    <div id="tab2">
      <div class="code-box">
        <div class="code-box__button">
          <button id="buttonCopy" class="button code-box__copy" type="button" data-func="copiar"><span class="icon-elem icon-elem--content_copy"></span>Copiar</button>
        </div>
        <div class="css-modificadores">
          <h6>Modificadores de estilos</h6>
          <ul class="listado-clases">
            <li><code class="clases">btn--principal</code></li>
          </ul>
        </div>
      </div>
    </div>
  </section>
</div>
<!--CODIGO-->
