<?php header("Content-Type: text/html;charset=utf-8"); ?>
<?php //include 'function.php';?>
<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Banco Estado</title>
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,700,700i" rel="stylesheet">
  <link rel="stylesheet" href="css/main.css">
  <link rel="stylesheet" href="css/be.css">
  <link rel="stylesheet" href="css/prettify.css"/>
  <link rel="stylesheet" href="css/banco_estado_pretty.css"/>
  <script>
    window.onload = (function(){ prettyPrint(); });
  </script>
<body>
  <header class="header-bar">
    <div class="brand">
      <a href="index.php" title="titulo de ejemplo">
        <img class="logo" src="images/logos/be.svg" alt="Logo">
      </a>
    </div>
      <div class="navegacion-lateral">
        <ul class="navegacion-lateral__menu">
        <!--  <li class="navegacion-lateral__item"><a href="#">c&#243;mo utilizar la librer&#237;a</a></li>-->

          <li class="navegacion-lateral__item--sub"><a href="#">gu&#237;a de interfaz</a>
            <ul class="in">
              <!--<li class="navegacion-lateral__item--child"><a href="#">Grilla</a></li>-->
              <li class="navegacion-lateral__item--child"><a href="alertas.php">Alertas</a></li>
              <li class="navegacion-lateral__item--child"><a href="botones.php">Botones</a></li>
              <li class="navegacion-lateral__item--child"><a href="colores.php">Color</a></li>
              <li class="navegacion-lateral__item--child"><a href="contenedores.php">M&#243;dulos</a></li>
              <li class="navegacion-lateral__item--child"><a href="formularios.php">Formularios</a></li>
              <li class="navegacion-lateral__item--child"><a href="navegacion.php">Navegaci&#243;n</a></li>
              <li class="navegacion-lateral__item--child"><a href="tablas.php">Tablas</a></li>
              <li class="navegacion-lateral__item--child"><a href="tipografia.php">Tipograf&#237;a</a></li>
              <li class="navegacion-lateral__item--child"><a href="ejemplos.php">Ejemplo Aplicado</a></li>
            </ul>
          </li>

          <!--  <li class="navegacion-lateral__item"><a href="#">gu&#237;a de redacci&#243;n</a></li>-->
          <!--  <li class="navegacion-lateral__item"><a href="#">recursos</a></li>-->
          <!--  <li class="navegacion-lateral__item"><a href="#">art&#237;culos</a></li>-->
          <!-- <li class="navegacion-lateral__item"><a href="#">dudas</a></li>-->
        </ul>
      </div>
  </header>
