<?php include 'header.php';?>
<main>
  <section class="content breadcrumbs-bar-frame">
    <?php include 'partials/breadcrumbs.php';?>
  </section>
  <section class="content">
  <div class="introduccion">
    <div class="title-section">
      <h1>Formularios</h1>
    </div>
    <p>Un formulario posibilita al usuario el env&#237;o de informaci&#243;n durante cualquier proceso. Su estructura debe facilitar esta labor, contando con opciones definidas y una disposici&#243;n clara al momento de ser visualizado. Para su construcci&#243;n es posible recurrir a m&#250;ltiples elementos como campos de texto, checkbox, botones u otros. Su utilizaci&#243;n, por &#250;ltimo, debe estar acorde a los colores, formatos, tamaños y tipograf&#237;as de esta gu&#237;a de estilos digitales.</p>
  </div>
</section>
  <div class="content-box">
    <section>
      <div class="item">
        <div class="title-section">
          <h2>Checkboxes</h2>
          <div class="divider"></div>
        </div>
        <p>Su principal funci&#243;n es permitir, por parte del usuario, la selecci&#243;n de una o varias opciones dentro del formulario. La cantidad de elementos de esta clase a desplegar en el sitio privado ser&#225; definida, principalmente, por el tipo de informaci&#243;n requerida.</p>
        <div class="tipogafria">
          <div class="agrupador-contenido">
            <form class="formulario">
              <label class="formulario__input">
                <input type="checkbox">
                <span class="formulario__input--checkbox"></span>
                <label class="formulario__label">Campo 1</label>
              </label>
            </form>
          </div>
          <div class="agrupador-contenido">
            <form class="formulario">
              <label class="formulario__input">
                <input type="checkbox" name="checkbox" value="checkbox">
                <span class="formulario__input--checkbox"></span>
                <label class="formulario__label">Campo 2</label>
              </label>
            </form>
          </div>
          <div class="agrupador-contenido">
            <form class="formulario">
              <label class="formulario__input formulario__input--desactivado">
                <input type="checkbox" name="checkbox" value="checkbox" disabled>
                <span class="formulario__input--checkbox"></span>
                <label class="formulario__label">Campo 3</label>
              </label>
            </form>
          </div>
        </div>
        <!--CODIGO-->
        <div class="wrapper">
          <ul class="tabs clearfix" data-tabgroup="formularios">
            <li><a href="#check-box-html"><span class="texto-desplegable">html</span><span class="icon-elem icon-elem--arrow_drop_down"></span></a></li>
            <li><a href="#check-box-css"><span class="texto-desplegable">css</span><span class="icon-elem icon-elem--arrow_drop_down"></span></a></li>
          </ul>
          <section id="checkboxs" class="tabgroup close">
            <div id="check-box-html">
              <div class="code-box">
                <pre id="copy-checkbox" class="prettyprint"><code class="lang-html">&#60;form class="formulario"&#62;
  &#60;label class="formulario__input"&#62;
    &#60;input type="checkbox"&#62;
    &#60;span class="formulario__input--checkbox"&#62;&#60;/span&#62;
    &#60;label class="formulario__label"&#62;Campo 1&#60;/label&#62;
  &#60;/label&#62;
&#60;/form&#62;<hr>
&#60;form class="formulario"&#62;
  &#60;label class="formulario__input"&#62;
    &#60;input type="checkbox" checked&#62;
    &#60;span class="formulario__input--checkbox"&#62;&#60;/span&#62;
    &#60;label class="formulario__label"&#62;Campo 2&#60;/label&#62;
  &#60;/label&#62;
&#60;/form&#62;<hr>
&#60;form class="formulario"&#62;
  &#60;label class="formulario__input formulario__input--desactivado"&#62;
    &#60;input type="checkbox" disabled&#62;
    &#60;span class="formulario__input--checkbox"&#62;&#60;/span&#62;
    &#60;label class="formulario__label"&#62;Campo 3&#60;/label&#62;
  &#60;/label&#62;
&#60;/form&#62;</code></pre>
                <div class="code-box__button">
                  <button data-clipboard-target="#copy-checkbox" class="button code-box__copy" type="button"><span class="icon-elem icon-elem--content_copy"></span>Copiar</button>
                </div>
              </div>
            </div>
            <div id="check-box-css">
              <div class="code-box">
                <div class="css-modificadores">
                  <h6>Modificadores de estilos</h6>
                  <ul class="listado-clases">
                    <li><code class="clases">formulario__input--checkbox</code></li>
                    <li><code class="clases">formulario__input--desactivado</code></li>
                  </ul>
                </div>
              </div>
            </div>
          </section>
        </div>
        <!--CODIGO-->
      </div>
      <div class="item">
        <div class="title-section">
          <h2>Radio button</h2>
          <div class="divider"></div>
        </div>
        <p>Con una funci&#243;n similar a un checkbox, se diferencia por su forma circular. Usualmente es utilizada cuando dentro de la selecci&#243;n el usuario debe marcar &#250;nicamente una preferencia. El n&#250;mero total de elementos depender&#225; de la informaci&#243;n a solicitar.</p>
        <div class="tipogafria">
          <div class="agrupador-contenido">
            <form class="formulario" action="index.html" method="post">
              <label class="formulario__input">
                <input type="radio" name="radio" value="radio">
                  <span class="formulario__input--radio"></span>
                <label class="formulario__label">Campo 1</label>
              </label>
            </form>
          </div>
          <div class="agrupador-contenido">
            <form class="formulario" action="index.html" method="post">
              <label class="formulario__input">
                <input type="radio" name="radio" value="radio" checked>
                <span class="formulario__input--radio"></span>
                <label class="formulario__label">Campo 2</label>
              </label>
            </form>
          </div>
          <div class="agrupador-contenido">
            <form class="formulario" action="index.html" method="post">
              <label class="formulario__input formulario__input--desactivado">
                <input type="radio" name="radio" value="radio" disabled>
                  <span class="formulario__input--radio"></span>
                <label class="formulario__label">Campo 3</label>
              </label>
            </form>
          </div>
        </div>
        <!--CODIGO-->
        <div class="wrapper">
          <ul class="tabs clearfix" data-tabgroup="formularios">
            <li><a href="#radio-html"><span class="texto-desplegable">html</span><span class="icon-elem icon-elem--arrow_drop_down"></span></a></li>
            <li><a href="#radio-css"><span class="texto-desplegable">css</span><span class="icon-elem icon-elem--arrow_drop_down"></span></a></li>
          </ul>
          <section id="radios" class="tabgroup close">
            <div id="radio-html">
              <div class="code-box">
                <pre id="copy-radio" class="prettyprint"><code class="lang-html">&#60;form class="formulario"&#62;
  &#60;label class="formulario__input"&#62;
    &#60;input type="radio"&#62;
    &#60;span class="formulario__input--radio"&#62;&#60;/span&#62;
    &#60;label class="formulario__label"&#62;Campo 1&#60;/label&#62;
  &#60;/label&#62;
&#60;/form&#62;<hr>
&#60;form class="formulario"&#62;
  &#60;label class="formulario__input"&#62;
    &#60;input type="radio" checked&#62;
    &#60;span class="formulario__input--radio"&#62;&#60;/span&#62;
    &#60;label class="formulario__label"&#62;Campo 2&#60;/label&#62;
  &#60;/label&#62;
&#60;/form&#62;<hr>
&#60;form class="formulario"&#62;
  &#60;label class="formulario__input formulario__input--desactivado"&#62;
    &#60;input type="radio" disabled&#62;
    &#60;span class="formulario__input--radio"&#62;&#60;/span&#62;
    &#60;label class="formulario__label"&#62;Campo 3&#60;/label&#62;
  &#60;/label&#62;
&#60;/form&#62;</code></pre>
                <div class="code-box__button">
                  <button data-clipboard-target="#copy-radio" class="button code-box__copy" type="button"><span class="icon-elem icon-elem--content_copy"></span>Copiar</button>
                </div>
              </div>
            </div>
            <div id="radio-css">
              <div class="code-box">
                <div class="css-modificadores">
                  <h6>Modificadores de estilos</h6>
                  <ul class="listado-clases">
                    <li><code class="clases">formulario__input--radio</code></li>
                    <li><code class="clases">formulario__input--desactivado</code></li>
                  </ul>
                </div>
              </div>
            </div>
          </section>
        </div>
        <!--CODIGO-->
      </div>
      <div class="item">
        <div class="title-section">
          <h2>Inputs</h2>
          <div class="divider"></div>
        </div>
        <p>La implementaci&#243;n de estos elementos en un formulario busca facilitar el ingreso de datos que el usuario debe escribir. Tambi&#233;n est&#225; la posibilidad de presentar un formato donde es posible seleccionar de una cantidad predefinida de opciones la necesaria.</p>
        <div class="tipogafria">
          <div class="agrupador-contenido">
            <div class="row">
              <div class="col-6">
                <form class="formulario">
                  <label class="formulario__input">
                    <input class="formulario__input--text" type="text" name="text" placeholder="Placeholder">
                  </label>
                </form>
              </div>
            </div>
          </div>
          <div class="agrupador-contenido">
            <div class="row">
              <div class="col-6">
                <form class="formulario">
                  <label class="formulario__input">
                    <select class="formulario__input--select" name="select">
                      <option value="1">Opcion</option>
                      <option value="2">Opcion</option>
                      <option value="3">Opcion</option>
                      <option value="4">Opcion</option>
                    </select>
                  </label>
                </form>
              </div>
            </div>
          </div>
        </div>
        <!--CODIGO-->
        <div class="wrapper">
          <ul class="tabs clearfix" data-tabgroup="formularios">
            <li><a href="#input-html"><span class="texto-desplegable">html</span><span class="icon-elem icon-elem--arrow_drop_down"></span></a></li>
            <li><a href="#input-css"><span class="texto-desplegable">css</span><span class="icon-elem icon-elem--arrow_drop_down"></span></a></li>
          </ul>
          <section id="radios" class="tabgroup close">
            <div id="input-html">
              <div class="code-box">
                <pre id="copy-inputs" class="prettyprint"><code class="lang-html">&#60;div class="row"&#62;
  &#60;div class="col-6"&#62;
    &#60;form class="formulario"&#62;
      &#60;label class="formulario__input"&#62;
        &#60;input class="formulario__input--text" type="text" name="text" placeholder="Placeholder"&#62;
      &#60;/label&#62;
    &#60;/form&#62;
  &#60;/div&#62;
&#60;/div&#62;<hr>
&#60;div class="row"&#62;
  &#60;div class="col-6"&#62;
    &#60;form class="formulario"&#62;
      &#60;label class="formulario__input"&#62;
        &#60;select class="formulario__input--select" name="select"&#62;
          &#60;option value="1"&#62;Opcion&#60;/option&#62;
          &#60;option value="2"&#62;Opcion&#60;/option&#62;
          &#60;option value="3"&#62;Opcion&#60;/option&#62;
          &#60;option value="4"&#62;Opcion&#60;/option&#62;
        &#60;/select&#62;
      &#60;/label&#62;
    &#60;/form&#62;
  &#60;/div&#62;
&#60;/div&#62;</code></pre>
                <div class="code-box__button">
                  <button data-clipboard-target="#copy-inputs" class="button code-box__copy" type="button"><span class="icon-elem icon-elem--content_copy"></span>Copiar</button>
                </div>
              </div>
            </div>
            <div id="input-css">
              <div class="code-box">
                <div class="css-modificadores">
                  <h6>Modificadores de estilos</h6>
                  <ul class="listado-clases">
                    <li><code class="clases">formulario__input--select</code></li>
                  </ul>
                </div>
              </div>
            </div>
          </section>
        </div>
        <!--CODIGO-->
      </div>
      <div class="item">
        <div class="title-section">
          <h2>Calendario</h2>
          <div class="divider"></div>
        </div>
        <p>La implementaci&#243;n de estos elementos en un formulario busca facilitar el ingreso de datos que el usuario debe escribir. Tambi&#233;n est&#225; la posibilidad de presentar un formato donde es posible seleccionar de una cantidad predefinida de opciones la necesaria.</p>
        <div class="tipogafria">
          <div class="agrupador-contenido">
            <div class="row">
              <div class="col-5">
                <label class="formulario__input formulario__input--calendario">
                  <input id="calendario" class="formulario__input--text" type="text" name="text" placeholder="Placeholder">
                </label>
              </div>
           </div>
          </div>
        </div>
        <!--CODIGO-->
        <div class="wrapper">
          <ul class="tabs clearfix" data-tabgroup="formularios">
            <li><a href="#calendario-html"><span class="texto-desplegable">html</span><span class="icon-elem icon-elem--arrow_drop_down"></span></a></li>
            <li><a href="#calendario-js"><span class="texto-desplegable">js</span><span class="icon-elem icon-elem--arrow_drop_down"></span></a></li>
          </ul>
          <section id="radios" class="tabgroup close">
            <div id="calendario-html">
              <div class="code-box">
                <pre id="copy-calendario" class="prettyprint"><code class="lang-html">&#60;div class="row"&#62;
  &#60;div class="col-5"&#62;
   &#60;form class="formulario"&#62;
    &#60;label class="formulario__input formulario__input--calendario"&#62;
     &#60;input id="calendario" class="formulario__input--text" type="text" name="text" placeholder="Placeholder"&#62;
    &#60;/label&#62;
  &#60;/form&#62;
 &#60;/div&#62;
&#60;/div&#62;</code></pre>
                <div class="code-box__button">
                  <button data-clipboard-target="#copy-calendario" class="button code-box__copy" type="button"><span class="icon-elem icon-elem--content_copy"></span>Copiar</button>
                </div>
              </div>
            </div>
            <div id="calendario-js">
              <div class="code-box">
                <pre id="copy-js-calendario" class="prettyprint"><code class="lang-js">$(document).ready(function()
{
 rome(calendario, { time: false });
});</code></pre>
            <div class="code-box__button">
              <button data-clipboard-target="#copy-js-calendario" id="buttonCopy" class="button code-box__copy" type="button"><span class="icon-elem icon-elem--content_copy"></span>Copiar</button>
            </div>
              </div>
            </div>
          </section>
        </div>
        <!--CODIGO-->
      </div>
      <div class="item">
        <div class="title-section">
          <h2>Barras de progreso</h2>
          <div class="divider"></div>
        </div>
        <p>Todo proceso dentro del sitio privado de BancoEstado que deba ejecutarse en varios pasos necesita mostrar su progreso. Para ello se puede implementar este tipo de soluciones, que permite detallar el estado de avance.</p>
        <div class="tipogafria">
          <div class="agrupador-contenido">
            <div class="barra-progreso">
              <ul class="barra-progreso__pasos">
                <li class="barra-progreso__paso barra-progreso__paso--activo">1. T&#237;tulo paso</li>
                <li class="barra-progreso__paso">2. T&#237;tulo paso</li>
                <li class="barra-progreso__paso">3. T&#237;tulo paso</li>
              </ul>
            </div>
            <div class="barra-progreso-mini">
              <ul class="barra-progreso__pasos">
                <li class="barra-progreso__paso barra-progreso__paso--activo">
                  1
                </li>
                <li class="barra-progreso__paso">
                  2
                </li>
                <li class="barra-progreso__paso">
                  3
                </li>
              </ul>
            </div>
          </div>
          <div class="agrupador-contenido">
            <div class="barra-progreso">
              <ul class="barra-progreso__pasos">
                <li class="barra-progreso__paso barra-progreso__paso--activo">1. T&#237;tulo paso</li>
                <li class="barra-progreso__paso barra-progreso__paso--activo">2. T&#237;tulo paso</li>
                <li class="barra-progreso__paso">3. T&#237;tulo paso</li>
              </ul>
            </div>
            <div class="barra-progreso-mini">
              <ul class="barra-progreso__pasos">
                <li class="barra-progreso__paso">
                  1
                </li>
                <li class="barra-progreso__paso barra-progreso__paso--activo">
                  2
                </li>
                <li class="barra-progreso__paso">
                  3
                </li>
              </ul>
            </div>
          </div>
          <div class="agrupador-contenido">
            <div class="barra-progreso">
              <ul class="barra-progreso__pasos">
                <li class="barra-progreso__paso barra-progreso__paso--activo">1. T&#237;tulo paso</li>
                <li class="barra-progreso__paso barra-progreso__paso--activo">2. T&#237;tulo paso</li>
                <li class="barra-progreso__paso barra-progreso__paso--activo">3. T&#237;tulo paso</li>
              </ul>
            </div>
            <div class="barra-progreso-mini">
              <ul class="barra-progreso__pasos">
                <li class="barra-progreso__paso">
                  1
                </li>
                <li class="barra-progreso__paso">
                  2
                </li>
                <li class="barra-progreso__paso barra-progreso__paso--activo">
                  3
                </li>
              </ul>
            </div>
          </div>    
        </div>
        <!--CODIGO-->
        <div class="wrapper">
          <ul class="tabs clearfix" data-tabgroup="formularios">
            <li><a href="#barra-html"><span class="texto-desplegable">html</span><span class="icon-elem icon-elem--arrow_drop_down"></span></a></li>
            <li><a href="#barra-css"><span class="texto-desplegable">css</span><span class="icon-elem icon-elem--arrow_drop_down"></span></a></li>
          </ul>
          <section id="barra-navegacion" class="tabgroup close">
            <div id="barra-html">
              <div class="code-box">
                <pre id="copy-barra" class="prettyprint"><code class="lang-html">&#60;div class="barra-progreso"&#62;
  &#60;ul class="barra-progreso__pasos"&#62;
    &#60;li class="barra-progreso__paso barra-progreso__paso--activo"&#62;
      1. T&#237;tulo paso
    &#60;/li&#62;
    &#60;li class="barra-progreso__paso"&#62;
      3. T&#237;tulo paso
    &#60;/li&#62;
    &#60;li class="barra-progreso__paso"&#62;
      3. T&#237;tulo paso
    &#60;/li&#62;
  &#60;/ul&#62;
&#60;/div&#62;<hr>
&#60;div class="barra-progreso"&#62;
  &#60;ul class="barra-progreso__pasos"&#62;
    &#60;li class="barra-progreso__paso barra-progreso__paso--activo"&#62;
      1. T&#237;tulo paso
    &#60;/li&#62;
    &#60;li class="barra-progreso__paso barra-progreso__paso--activo"&#62;
      3. T&#237;tulo paso
    &#60;/li&#62;
    &#60;li class="barra-progreso__paso"&#62;
      3. T&#237;tulo paso
    &#60;/li&#62;
  &#60;/ul&#62;
&#60;/div&#62;<hr>
&#60;div class="barra-progreso"&#62;
  &#60;ul class="barra-progreso__pasos"&#62;
    &#60;li class="barra-progreso__paso barra-progreso__paso--activo"&#62;
      1. T&#237;tulo paso
    &#60;/li&#62;
    &#60;li class="barra-progreso__paso barra-progreso__paso--activo"&#62;
      3. T&#237;tulo paso
    &#60;/li&#62;
    &#60;li class="barra-progreso__paso barra-progreso__paso--activo"&#62;
      3. T&#237;tulo paso
    &#60;/li&#62;
  &#60;/ul&#62;
&#60;/div&#62;</code></pre>
                <div class="code-box__button">
                  <button data-clipboard-target="#copy-barra" class="button code-box__copy" type="button"><span class="icon-elem icon-elem--content_copy"></span>Copiar</button>
                </div>
              </div>
            </div>
            <div id="barra-css">
              <div class="code-box">
                <div class="css-modificadores">
                  <h6>Modificadores de estilos</h6>
                  <ul class="listado-clases">
                    <li><code class="clases">barra-progreso__paso--activo</code></li>
                  </ul>
                </div>
              </div>
            </div>
          </section>
        </div>
        <!--CODIGO-->
      </div>
      <div class="item">
        <div class="title-section">
          <h3>Barra de progreso (responsiva)</h3>
          <div class="divider"></div>
        </div>
        <div class="tipogafria">
          <div class="agrupador-contenido">
            <div class="boton-descarga">
              <a href="descarga/diseno_mejoras/barra.pdf" target="_blank"><button class="btn btn--principal"><span class="icon-elem icon-elem--file_download"></span>Descargar</button></a>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
<?php include 'footer.php';?>
</main>
<script type="text/javascript">
  //calendario
  rome(calendario, { time: false });
</script>
