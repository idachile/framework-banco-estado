<?php
$url = $_SERVER["PHP_SELF"];
$url = str_replace('/','',$url);
$url = explode('.',$url);
$url = $url[0];

?>
<div class="gridle-row">
  <div class="gridle-gr-3 gridle-gr-12@tablet">
    <ul class="breadcrumb">
      <li class="breadcrumb__item">
        <a href="/">Inicio</a>
      </li>
      <li class="breadcrumb__item breadcrumb__item--activo">
        <?php echo $url; ?>
      </li>
    </ul>
  </div>
  <div class="gridle-gr-9 gridle-gr-12@tablet download-box">
    <div class="boton-descarga">
      <a href="descarga/starter_pack/starter_pack.zip" download="Starter Pack"><button class="btn btn--principal"><span class="icon-elem icon-elem--file_download"></span>Descargar pack</button></a>
    </div>
  </div>
</div>
