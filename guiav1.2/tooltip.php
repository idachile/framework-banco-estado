<?php include 'header.php';?>
<main>
  <section class="content breadcrumbs-bar-frame">
    <?php include 'partials/breadcrumbs.php';?>
  </section>
  <section class="content">
  <div class="introduccion">
    <div class="title-section">
      <h1 class="m-0">Tooltip</h1>
      <p>Los m&#243;dulos son espacios espec&#237;ficos dentro del sitio privado de BancoEstado destinados a entregar y/o solicitar informaci&#243;n al usuario. Se diferencian del resto de la p&#225;gina gracias al uso de los colores definidos previamente en la paleta. En su interior pueden contener textos, campos para su ingreso, botones o una combinaci&#243;n de ellos.</p>
    </div>
  </div>
</section>
  <div class="content-box">
    <section>
      <div class="item item--tooltip">
        <div class="tipogafria">
          <div class="agrupador-contenido">
            <div tooltip="Lorem ipsum dolor sit amet, consectetur adipisicing elit.
            Temporibus hic similique voluptas nam praesentium et numquam ratione." posicion="izquierda">
              <span class="icono icono--tooltip"></span>
            </div>
          </div>
          <div class="agrupador-contenido">
            <div tooltip="Lorem ipsum dolor sit amet, consectetur adipisicing elit.
            Temporibus hic similique voluptas nam praesentium et numquam ratione." posicion="arriba">
              <span class="icono icono--tooltip"></span>
            </div>
          </div>
          <div class="agrupador-contenido">
            <div tooltip="Lorem ipsum dolor sit amet, consectetur adipisicing elit.
            Temporibus hic similique voluptas nam praesentium et numquam ratione." posicion="derecha">
              <span class="icono icono--tooltip"></span>
            </div>
          </div>
          <div class="agrupador-contenido">
            <div tooltip="Lorem ipsum dolor sit amet, consectetur adipisicing elit.
            Temporibus hic similique voluptas nam praesentium et numquam ratione." posicion="abajo">
              <span class="icono icono--tooltip"></span>
            </div>
          </div>
        </div>
        <!--CODIGO-->
        <div class="wrapper">
          <ul class="tabs clearfix" data-tabgroup="breadcrumbs">
            <li><a href="#breadcrumbs-html"><span class="texto-desplegable">html</span><span class="icon-elem icon-elem--arrow_drop_down"></span></a></li>
          </ul>
          <section id="breadcrumbs" class="tabgroup close">
            <div id="breadcrumbs-html">
              <div class="code-box">
                <pre id="copy-tooltip" class="prettyprint"><code class="lang-html">&#60;div tooltip="Lorem ipsum dolor sit amet, consectetur adipisicing elit.
Temporibus hic similique voluptas nam praesentium et numquam ratione." posicion="derecha"&#62;
  &#60;span class="icono icono--tooltip""&#62;
&#60;/div&#62;<hr>
&#60;div tooltip="Lorem ipsum dolor sit amet, consectetur adipisicing elit.
Temporibus hic similique voluptas nam praesentium et numquam ratione." posicion="arriba"&#62;
  &#60;span class="icono icono--tooltip""&#62;
&#60;/div&#62;<hr>
&#60;div tooltip="Lorem ipsum dolor sit amet, consectetur adipisicing elit.
Temporibus hic similique voluptas nam praesentium et numquam ratione." posicion="izquierda"&#62;
  &#60;span class="icono icono--tooltip""&#62;
&#60;/div&#62;<hr>
&#60;div tooltip="Lorem ipsum dolor sit amet, consectetur adipisicing elit.
Temporibus hic similique voluptas nam praesentium et numquam ratione." posicion="abajo"&#62;
  &#60;span class="icono icono--tooltip""&#62;
&#60;/div&#62;<hr></code></pre>
                <div class="code-box__button">
                  <button class="button code-box__copy" type="button"><span class="icon-elem icon-elem--content_copy"></span>Copiar</button>
                </div>
              </div>
            </div>
          </section>
        </div>
        <!--CODIGO-->
      </div>
    </section>
  </div>
  <?php include 'footer.php';?>
</main>
