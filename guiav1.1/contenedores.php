<?php include 'header.php';?>
<main>
  <section class="content breadcrumbs-bar-frame">
    <?php include 'partials/breadcrumbs.php';?>
  </section>
  <section class="content">
  <div class="introduccion">
    <div class="title-section">
      <h1>m&#243;dulos</h1>
    </div>
    <p>Los m&#243;dulos son espacios espec&#237;ficos dentro del sitio privado de BancoEstado destinados a entregar y/o solicitar informaci&#243;n al usuario. Se diferencian del resto de la p&#225;gina gracias al uso de los colores definidos previamente en la paleta. En su interior pueden contener textos, campos para su ingreso, botones o una combinaci&#243;n de ellos.</p>
  </div>
</section>
  <div class="content-box">
    <section>
      <div class="item">
        <div class="title-section">
          <h2>Modulo principal</h2>
          <div class="divider"></div>
        </div>
        <p>Su implementaci&#243;n destaca por contar con un fondo de color, dentro de las opciones disponibles, lo que marca una clara diferencia con el resto del wireframe. Durante su implementaci&#243;n es posible añadir con texto o botones.</p>
        <div class="tipogafria">
          <div class="agrupador-contenido">
            <div class="modulo">
              <h6 class="modulo__titulo">Inscribe tus cuentas</h6>
              <p class="modulo__mensaje">Inscribe tus cuentas para pagarlas mes a mes. Tus cuentas aparecer&#225;n aqu&#237; para que puedas pagarlas directamente a trav&#233;s del Banco</p>
              <!--tooltip-->
              <div tooltip="Lorem ipsum dolor sit amet, consectetur adipisicing elit.
              Temporibus hic similique voluptas nam praesentium et numquam ratione." posicion="derecha">
                <span class="icono icono--tooltip"></span>
              </div>
              <!--tooltip-->
              <button class="btn btn--principal" type="button" name="button">Inscribir cuentas</button>
            </div>
          </div>
        </div>
        <!--CODIGO-->
        <div class="wrapper">
          <ul class="tabs clearfix" data-tabgroup="btn-secundario">
            <li><a href="#modulo-principal"><span class="texto-desplegable">html</span><span class="icon-elem icon-elem--arrow_drop_down"></span></a></li>
          </ul>
          <section id="btn-secundario" class="tabgroup close">
            <div id="modulo-principal">
              <div class="code-box">
                  <pre id="copy-modulo-principal" class="prettyprint"><code class="lang-html">&#60;div class="modulo"&#62;<br>
  &#60;h6 class="modulo__titulo"&#62;Inscribe tus cuentas&#60;/p&#62;<br>
  &#60;p class="modulo__mensaje"&#62;
    Inscribe tus cuentas para pagarlas mes a mes. Tus cuentas aparecer&#225;n aqu&#237;
    para que puedas pagarlas directamente a trav&#233;s del Banco<br>
    &#60;span class="icono icono--tooltip tooltip"&#62;<br>
      &#60;span class="tooltiptext"&#62;<br>
        Lorem ipsum dolor sit amet, consectetur adipisicing elit.<br>
        Temporibus hic similique voluptas nam praesentium et numquam ratione.<br>
      &#60;/span&#62;<br>
    &#60;/span&#62;<br>
  &#60;/p&#62;<br>
  &#60;button class="btn btn--principal" type="button" name="button"&#62;
    Inscribir cuentas
  &#60;/button&#62;<br>
&#60;/div&#62;<br></code></pre>
                <div class="code-box__button">
                  <button data-clipboard-target="#copy-modulo-principal" class="button code-box__copy" type="button"><span class="icon-elem icon-elem--content_copy"></span>Copiar</button>
                </div>
              </div>
            </div>
          </section>
        </div>
        <!--CODIGO-->
      </div>
      <div class="item">
        <div class="title-section">
          <h2>Modulo secundario</h2>
          <div class="divider"></div>
        </div>
        <p>Al igual que el principal, tambi&#233;n utiliza uno de los tonos presentes en la paleta de colores para separar contenido espec&#237;fico. La &#250;nica diferencia es que resalta en el wireframe usando un marco. Puede llevar texto, botones o ambos.</p>
        <div class="tipogafria">
          <div class="agrupador-contenido">
            <div class="modulo modulo--secundario">
              <h6 class="modulo__titulo">Inscribe tus cuentas</h6>
              <p class="modulo__mensaje">Inscribe tus cuentas para pagarlas mes a mes. Tus cuentas aparecer&#225;n aqu&#237; para que puedas pagarlas directamente a trav&#233;s del Banco
                <!--tooltip-->
                <div tooltip="Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                Temporibus hic similique voluptas nam praesentium et numquam ratione." posicion="derecha">
                  <span class="icono icono--tooltip"></span>
                </div>
                <!--tooltip-->
              </p>
              <button class="btn btn--principal btn--borde" type="button" name="button">Inscribir cuentas</button>
            </div>
          </div>
        </div>
        <!--CODIGO-->
        <div class="wrapper">
          <ul class="tabs clearfix" data-tabgroup="btn-secundario">
            <li><a href="#modulo-secundario"><span class="texto-desplegable">html</span><span class="icon-elem icon-elem--arrow_drop_down"></span></a></li>
          </ul>
          <section id="btn-secundario" class="tabgroup close">
            <div id="modulo-secundario">
              <div class="code-box">
                  <pre id="copy-modulo-secundario" class="prettyprint"><code class="lang-html">&#60;div class="modulo modulo--secundario"&#62;
  &#60;h6 class="modulo__titulo"&#62;Inscribe tus cuentas&#60;/p&#62;
  &#60;p class="modulo__mensaje"&#62;
    Inscribe tus cuentas para pagarlas mes a mes.
    Tus cuentas aparecer&#225;n aqu&#237; para que puedas pagarlas directamente a trav&#233;s del Banco
    &#60;span class="icono icono--tooltip tooltip"&#62;
      &#60;span class="tooltiptext tooltiptext--top"&#62;
        Lorem ipsum dolor sit amet, consectetur adipisicing elit.
        Temporibus hic similique voluptas nam praesentium et numquam ratione.
      &#60;/span&#62;
    &#60;/span&#62;
  &#60;/p&#62;
  &#60;button class="btn btn--principal btn--borde" type="button" name="button"&#62;
    Inscribir cuentas
  &#60;/button&#62;
&#60;/div&#62;</code></pre>
                <div class="code-box__button">
                  <button data-clipboard-target="#copy-modulo-secundario" class="button code-box__copy" type="button"><span class="icon-elem icon-elem--content_copy"></span>Copiar</button>
                </div>
              </div>
            </div>
          </section>
        </div>
        <!--CODIGO-->
      </div>
      <div class="item">
        <div class="title-section">
          <h2>M&#243;DULO PARA TEXTO</h2>
          <div class="divider"></div>
        </div>
        <p>La funci&#243;n que cumple dentro del sitio privado de BancoEstado est&#225; dada por la necesidad de recibir un dato ingresado por el usuario. Su estructura consiste en texto (con los par&#225;metros de esta gu&#237;a de estilos) y un campo para ingresar la informaci&#243;n.</p>
        <div class="tipogafria">
          <div class="agrupador-contenido">
            <div class="modulo">
              <div class="row">
                <div class="col-6">
                  <h6 class="modulo__titulo">Inscribe tus cuentas</h6>
                  <p class="modulo__mensaje">Inscribe tus cuentas para pagarlas mes a mes. Tus cuentas aparecer&#225;n aqu&#237; para que puedas pagarlas directamente a trav&#233;s del Banco</p>
                </div>
                <div class="col-6">
                  <form class="formulario" action="index.html" method="post">
                    <label class="formulario__input">
                      <input class="formulario__input--text" type="text" name="text" placeholder="Placeholder">
                      <p class="formulario__footer"><small>El mail debe contener el caracter “@“. <strong>Ej: mail@mail.cl</strong></small></p>
                    </label>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!--CODIGO-->
        <div class="wrapper">
          <ul class="tabs clearfix" data-tabgroup="btn-secundario">
            <li><a href="#modulo-input"><span class="texto-desplegable">html</span><span class="icon-elem icon-elem--arrow_drop_down"></span></a></li>
          </ul>
          <section id="btn-secundario" class="tabgroup close">
            <div id="modulo-input">
              <div class="code-box">
                  <pre id="copy-modulo-doble" class="prettyprint"><code class="lang-html">&#60;div class="modulo"&#62;
  &#60;div class="row"&#62;
    &#60;div class="col-6"&#62;
      &#60;h6 class="modulo__titulo"&#62;Inscribe tus cuentas&#60;/p&#62;
      &#60;p class="modulo__mensaje"&#62;
        Inscribe tus cuentas para pagarlas mes a mes. Tus cuentas aparecer&#225;n aqu&#237; para que puedas
        pagarlas directamente a trav&#233;s del Banco
        &#60;span class="icono icono--tooltip tooltip"&#62;
          &#60;span class="tooltiptext tooltiptext--left"&#62;
            Lorem ipsum dolor sit amet, consectetur adipisicing elit.
            Temporibus hic similique voluptas nam praesentium et numquam ratione.
          &#60;/span&#62;
        &#60;/span&#62;
      &#60;/p&#62;
      &#60;button class="btn btn--principal btn--borde" type="button" name="button"&#62;Inscribir cuentas&#60;/button&#62;
    &#60;/div&#62;
    &#60;div class="col-6"&#62;
      &#60;form class="formulario"&#62;
        &#60;label class="formulario__input"&#62;
          &#60;input class="formulario__input--text" type="text" name="text" placeholder="Placeholder"&#62;
          &#60;p class="formulario__footer"&#62;El mail debe contener el caracter “@“. &#60;strong&#62;Ej: mail@mail.cl&#60;/strong&#62;&#60;/p&#62;
        &#60;/label&#62;
      &#60;/form&#62;
    &#60;/div&#62;
  &#60;/div&#62;</code></pre>
                <div class="code-box__button">
                  <button data-clipboard-target="#copy-modulo-doble" class="button code-box__copy" type="button"><span class="icon-elem icon-elem--content_copy"></span>Copiar</button>
                </div>
              </div>
            </div>
          </section>
        </div>
        <!--CODIGO-->
      </div>
      <div class="item">
        <div class="title-section">
          <h2>M&#243;DULO PARA TEXTO CON LLAMADO DE ACCI&#243;N</h2>
          <div class="divider"></div>
        </div>
        <p>En el sitio privado este componente tiene la finalidad de recibir datos entregados por el usuario y permitir su procesamiento. Para implementarlo se requiere una descripci&#243;n de la acci&#243;n a realizar, un campo de texto y el bot&#243;n respectivo.</p>
        <div class="tipogafria">
          <div class="agrupador-contenido">
            <div class="modulo">
                <h6 class="modulo__titulo">Inscribe tus cuentas</h6>
                <p class="modulo__mensaje">Inscribe tus cuentas para pagarlas mes a mes. Tus cuentas aparecer&#225;n aqu&#237; para que puedas pagarlas directamente a trav&#233;s del Banco
                  <!--tooltip-->
                  <div tooltip="Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                  Temporibus hic similique voluptas nam praesentium et numquam ratione." posicion="derecha">
                    <span class="icono icono--tooltip"></span>
                  </div>
                  <!--tooltip-->
                </p>
                <form class="formulario" action="index.html" method="post">
                  <div class="row">
                    <div class="col-8">
                      <label class="formulario__input">
                        <input class="formulario__input--text" type="text" name="text" placeholder="Placeholder">
                        <p class="formulario__footer"><small>El mail debe contener el caracter “@“. Ej: mail@mail.cl</small></p>
                      </label>
                    </div>
                    <div class="col-4">
                      <button class="btn btn--principal" type="button" name="button">Inscribir cuentas</button>
                    </div>
                  </div>
                </form>
            </div>
          </div>
        </div>
        <!--CODIGO-->
        <div class="wrapper">
          <ul class="tabs clearfix" data-tabgroup="btn-secundario">
            <li><a href="#modulo-btn"><span class="texto-desplegable">html</span><span class="icon-elem icon-elem--arrow_drop_down"></span></a></li>
          </ul>
          <section id="btn-secundario" class="tabgroup close">
            <div id="modulo-btn">
              <div class="code-box">
                  <pre id="copy-modulo-x" class="prettyprint"><code class="lang-html">&#60;div class="modulo"&#62;
  &#60;h6 class="modulo__titulo"&#62;Inscribe tus cuentas&#60;/p&#62;
  &#60;p class="modulo__mensaje"&#62;Inscribe tus cuentas para pagarlas mes a mes. Tus cuentas aparecer&#225;n aqu&#237; para que puedas pagarlas directamente a trav&#233;s del Banco
    &#60;span class="icono icono--tooltip tooltip"&#62;
      &#60;span class="tooltiptext tooltiptext--bottom"&#62;
        Lorem ipsum dolor sit amet, consectetur adipisicing elit.
        Temporibus hic similique voluptas nam praesentium et numquam ratione.
      &#60;/span&#62;
    &#60;/span&#62;
  &#60;/p&#62;
  &#60;form class="formulario"&#62;
    &#60;div class="row"&#62;
      &#60;div class="col-6"&#62;
        &#60;label class="formulario__input"&#62;
          &#60;input class="formulario__input--text" type="text" name="text" placeholder="Placeholder"&#62;
          &#60;p class="formulario__footer"&#62;
            El mail debe contener el caracter “@“.
              &#60;strong&#62;
                Ej: mail@mail.cl
              &#60;/strong&#62;
          &#60;/p&#62;
        &#60;/label&#62;
      &#60;/div&#62;
      &#60;div class="col-6"&#62;
        &#60;button class="btn btn--principal btn--borde" type="button" name="button"&#62;
          Inscribir cuentas
        &#60;/button&#62;
      &#60;/div&#62;
    &#60;/div&#62;
  &#60;/form&#62;
&#60;/div&#62;</code></pre>
                <div class="code-box__button">
                  <button data-clipboard-target="#copy-modulo-x" class="button code-box__copy" type="button"><span class="icon-elem icon-elem--content_copy"></span>Copiar</button>
                </div>
              </div>
            </div>
          </section>
        </div>
        <!--CODIGO-->
      </div>
      <div class="item">
        <div class="title-section">
          <h2>M&#243;DULO PARA DATOS DE COMPROBANTE</h2>
          <div class="divider"></div>
        </div>
        <p>Permite visualizar informaci&#243;n luego de cualquier operaci&#243;n realizada por el usuario. Para ello despliega texto (datos personales, bancarios, etc.) En su interior tambi&#233;n puede contener diversos botones que llamen a acciones determinadas.</p>
        <div class="tipogafria">
          <div class="agrupador-contenido">
            <div class="modulo modulo--accion">
              <h6 class="modulo__titulo">Datos y forma de pago</h6>
              <div class="row">
                <div class="col-4">
                  <div class="datos">
                    <p class="datos__rotulo">cliente</p>
                    <p class="datos__valor">Eduardo Farias Riquelme</p>
                  </div>
                  <div class="datos">
                    <p class="datos__rotulo">producto</p>
                    <p class="datos__valor">Cuenta corriente</p>
                  </div>
                  <div class="datos">
                    <p class="datos__rotulo">fecha</p>
                    <p class="datos__valor">26/03/2017</p>
                  </div>
                </div>
                <div class="col-4">
                  <div class="datos">
                    <p class="datos__rotulo">rut</p>
                    <p class="datos__valor">15.455.456-3</p>
                  </div>
                  <div class="datos">
                    <p class="datos__rotulo">n&#250;mero de operaci&#243;n</p>
                    <p class="datos__valor">00211521151215812</p>
                  </div>
                  <div class="datos">
                    <p class="datos__rotulo">hora</p>
                    <p class="datos__valor">14:29</p>
                  </div>
                </div>
                <div class="col-4">
                  <div class="datos">
                    <div class="comprobante__sello">sello</div>
                  </div>
                </div>
              </div>
              <div class="modulo__divisor"></div>
              <h6 class="modulo__titulo">Detalle de cuenta pagada</h6>
              <div class="row">
                <div class="col-4">
                  <div class="datos">
                    <p class="datos__rotulo">empresa recaudadora</p>
                    <p class="datos__valor">Aguas Andinas</p>
                  </div>
                  <div class="datos">
                    <p class="datos__rotulo">monto pagado</p>
                    <p class="datos__valor">$20.000</p>
                  </div>
                </div>
                <div class="col-4">
                  <div class="datos">
                    <p class="datos__rotulo">nombre cuenta</p>
                    <p class="datos__valor">Agua Casa</p>
                  </div>
                  <div class="datos">
                    <p class="datos__rotulo">n&#250;mero de documento</p>
                    <p class="datos__valor">00211521151215812</p>
                  </div>
                </div>
                <div class="col-4">
                  <div class="datos">
                    <div class="datos">
                      <p class="datos__rotulo">identificador de cliente</p>
                      <p class="datos__valor">88985633</p>
                    </div>
                    <div class="datos">
                      <p class="datos__rotulo">vencimiento</p>
                      <p class="datos__valor">26/03/2017</p>
                    </div>
                  </div>
                </div>
              </div>
              <div class="modulo__impresion">
                <button class="btn btn--principal" type="button" name="button"><span class="icono icono--pdf"></span>Descargar PDF</button>
                <button class="btn btn--principal btn--borde" type="button" name="button"><span class="icono icono--imprimir"></span>Imprimir comprobante</button>
                <button class="btn btn--principal btn--borde" type="button" name="button"><span class="icono icono--mail"></span>Enviar por Email</button>
              </div>
            </div>
          </div>
        </div>
        <!--CODIGO-->
        <div class="wrapper">
          <ul class="tabs clearfix" data-tabgroup="modulos">
            <li><a href="#modulo-comprobante"><span class="texto-desplegable">html</span><span class="icon-elem icon-elem--arrow_drop_down"></span></a></li>
          </ul>
          <section id="modulo-comprobante1" class="tabgroup close">
            <div id="modulo-comprobante">
              <div class="code-box">
                  <pre id="copy-modulo-comprobante" class="prettyprint"><code class="lang-html">&#60;div class="modulo"&#62;
  &#60;h6 class="modulo__titulo"&#62;Datos y forma de pago&#60;/p&#62;
  &#60;div class="row"&#62;
    &#60;div class="col-4"&#62;
      &#60;div class="datos"&#62;
        &#60;p class="datos__rotulo"&#62;cliente&#60;/p&#62;
        &#60;p class="datos__valor"&#62;Eduardo Farias Riquelme&#60;/p&#62;
      &#60;/div&#62;
      &#60;div class="datos"&#62;
        &#60;p class="datos__rotulo"&#62;producto&#60;/p&#62;
        &#60;p class="datos__valor"&#62;Eduardo Farias Riquelme&#60;/p&#62;
      &#60;/div&#62;
      &#60;div class="datos"&#62;
        &#60;p class="datos__rotulo"&#62;fecha&#60;/p&#62;
        &#60;p class="datos__valor"&#62;Eduardo Farias Riquelme&#60;/p&#62;
      &#60;/div&#62;
    &#60;/div&#62;
    &#60;div class="col-4"&#62;
      &#60;div class="datos"&#62;
        &#60;p class="datos__rotulo"&#62;rut&#60;/p&#62;
        &#60;p class="datos__valor"&#62;numero de operacion&#60;/p&#62;
      &#60;/div&#62;
      &#60;div class="datos"&#62;
        &#60;p class="datos__rotulo"&#62;numero de operacion&#60;/p&#62;
        &#60;p class="datos__valor"&#62;Eduardo Farias Riquelme&#60;/p&#62;
      &#60;/div&#62;
      &#60;div class="datos"&#62;
        &#60;p class="datos__rotulo"&#62;hora&#60;/p&#62;
        &#60;p class="datos__valor"&#62;Eduardo Farias Riquelme&#60;/p&#62;
      &#60;/div&#62;
    &#60;/div&#62;
    &#60;div class="col-4"&#62;
      &#60;div class="datos"&#62;
        &#60;div class="datos__sello"&#62;sello&#60;/div&#62;
      &#60;/div&#62;
    &#60;/div&#62;
  &#60;/div&#62;
  &#60;div class="modulo__divisor"&#62;&#60;/div&#62;
  &#60;h6 class="modulo__titulo"&#62;Detalles de cuenta pagada&#60;/p&#62;
  &#60;div class="row"&#62;
    &#60;div class="col-4"&#62;
      &#60;div class="datos"&#62;
        &#60;p class="datos__rotulo"&#62;empresa recaudadora#60;/p&#62;
        &#60;p class="datos__valor"&#62;Aguas Andinas&#60;/p&#62;
      &#60;/div&#62;
      &#60;div class="datos"&#62;
        &#60;p class="datos__rotulo"&#62;monto pagado&#60;/p&#62;
        &#60;p class="datos__valor"&#62;$20.000&#60;/p&#62;
      &#60;/div&#62;
    &#60;/div&#62;
    &#60;div class="col-4"&#62;
      &#60;div class="datos"&#62;
        &#60;p class="datos__rotulo"&#62;nombre cuenta&#60;/p&#62;
        &#60;p class="datos__valor"&#62;Agua Casa&#60;/p&#62;
      &#60;/div&#62;
      &#60;div class="datos"&#62;
        &#60;p class="datos__rotulo"&#62;n&#250;mero de documento&#60;/p&#62;
        &#60;p class="datos__valor"&#62;00211521151215812&#60;/p&#62;
      &#60;/div&#62;
    &#60;/div&#62;
    &#60;div class="col-4"&#62;
      &#60;div class="datos"&#62;
        &#60;p class="datos__rotulo"&#62;identificador de cliente&#60;/p&#62;
        &#60;p class="datos__valor"&#62;88985633&#60;/p&#62;
      &#60;/div&#62;
      &#60;div class="datos"&#62;
        &#60;p class="datos__rotulo"&#62;vencimiento&#60;/p&#62;
        &#60;p class="datos__valor"&#62;26/03/2017&#60;/p&#62;
      &#60;/div&#62;
    &#60;/div&#62;
  &#60;/div&#62;
  &#60;div class="modulo__impresion"&#62;
    &#60;button class="btn btn--principal" type="button" name="button"&#62;
      &#60;span class="icono icono--pdf"&#62;&#60;/span&#62;Descargar PDF
    &#60;/button&#62;
    &#60;button class="btn btn--borde" type="button" name="button"&#62;
      &#60;span class="icono icono--imprimir"&#62;&#60;/span&#62;Imprimir comprobante
    &#60;/button&#62;
    &#60;button class="btn btn--borde" type="button" name="button"&#62;
      &#60;span class="icono icono--mail"&#62;&#60;/span&#62;Enviar por Email
    &#60;/button&#62;
  &#60;/div&#62;
&#60;/div&#62;</code></pre>
                <div class="code-box__button">
                  <button data-clipboard-target="#copy-modulo-comprobante" class="button code-box__copy" type="button"><span class="icon-elem icon-elem--content_copy"></span>Copiar</button>
                </div>
              </div>
            </div>
          </section>
        </div>
        <!--CODIGO-->
      </div>
      <div class="item">
        <div class="title-section">
          <h3>Barra de progreso (responsiva)</h3>
          <div class="divider"></div>
        </div>
        <div class="tipogafria">
          <div class="agrupador-contenido">
            <div class="boton-descarga">
              <a href="descarga/diseno_mejoras/modulos.pdf" target="_blank"><button class="btn btn--principal"><span class="icon-elem icon-elem--file_download"></span>Descargar</button></a>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
<?php include 'footer.php';?>
</main>
