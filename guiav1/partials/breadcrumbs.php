<?php
$url = $_SERVER["PHP_SELF"];
$url = str_replace('/','',$url);
$url = explode('.',$url);
$url = $url[0];

?>
<div class="gridle-row">
  <div class="gridle-gr-3">
    <ul class="breadcrumb">
      <li class="breadcrumb__item">
        <a href="/">Inicio</a>
      </li>
      <li class="breadcrumb__item breadcrumb__item--activo">
        <?php echo $url; ?>
      </li>
    </ul>
  </div>
  <div class="gridle-gr-9 download-box">
    <div class="boton-descarga">
      <button class="btn btn--principal"><span class="icon-elem icon-elem--file_download"></span>Descargar pack (html,css,js)</button>
    </div>
  </div>
</div>
