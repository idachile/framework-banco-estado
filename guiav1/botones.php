<?php include 'header.php';?>
<main>
  <section class="content breadcrumbs-bar-frame">
    <?php include 'partials/breadcrumbs.php';?>
  </section>
  <section class="content">
  <div class="introduccion">
    <div class="title-section">
      <h1>Botones</h1>
    </div>
    <p>Todo bot&#243;n al interior del sitio privado cumple una funci&#243;n espec&#237;fica, la que estar&#225; determinada por la operaci&#243;n que necesite realizar el usuario. Al igual que los otros componentes presentes en la plataforma, su implementaci&#243;n est&#225; definida por la paleta de colores y tipograf&#237;a ya estandarizadas en esta gu&#237;a de estilos digitales.</p>
  </div>
</section>
  <div class="content-box">
    <section>
      <div class="item">
        <div class="title-section">
          <h2>Principal</h2>
          <div class="divider"></div>
        </div>
        <p>Es la variante m&#225;s sencilla del set de botones disponibles. Una vez implementado, al ser utilizado muestra un leve cambio en su color, siempre respetando los tonos disponibles.</p>
        <div class="tipogafria">
          <div class="agrupador-contenido">
            <button class="btn btn--principal" type="button" name="button">Bot&#243;n Primario</button>
          </div>
        </div>
        <!--CODIGO-->
        <div class="wrapper">
          <ul class="tabs clearfix" data-tabgroup="btn-basico">
            <li><a href="#tab1"><span class="texto-desplegable">html</span><span class="icon-elem icon-elem--arrow_drop_down"></span></a></li>
          </ul>
          <section id="btn-basico" class="tabgroup close">
            <div id="tab1">
              <div class="code-box">
                <div class="code-box__button">
                  <button class="button code-box__copy" type="button" onclick="copyToClipboard('#cp-btn-principal')"><span class="icon-elem icon-elem--content_copy"></span>Copiar</button>
                </div>
                <div class="code-box__code">
                  <pre id="cp-btn-principal" class="prettyprint linenums"><code class="lang-html">&#60;button class="btn btn--principal" type="button" name="button"&#62;Bot&#243;n principal&#60;/button&#62;</code></pre>
                </div>
              </div>
            </div>
          </section>
        </div>
        <!--CODIGO-->
      </div>
      <div class="item">
        <div class="title-section">
          <h2>Secundario</h2>
          <div class="divider"></div>
        </div>
        <p>Su utilizaci&#243;n en el sitio privado de BancoEstado se aplica a contextos donde se necesita, por ejemplo, generar un contraste con el resto de los contenidos de una p&#225;gina determinada.</p>
        <div class="tipogafria">
          <div class="agrupador-contenido">
            <button class="btn btn--borde" type="button" name="button">Bot&#243;n Secundario</button>
          </div>
        </div>
        <!--CODIGO-->
        <div class="wrapper">
          <ul class="tabs clearfix" data-tabgroup="btn-secundario">
            <li><a href="#tab3"><span class="texto-desplegable">html</span><span class="icon-elem icon-elem--arrow_drop_down"></span></a></li>
          </ul>
          <section id="btn-secundario" class="tabgroup close">
            <div id="tab3">
              <div class="code-box">
                <div class="code-box__button">
                  <button class="button code-box__copy" type="button" onclick="copyToClipboard('#cp-btn-secundario')"><span class="icon-elem icon-elem--content_copy"></span>Copiar</button>
                </div>
                  <pre id="cp-btn-secundario" class="prettyprint linenums"><code class="lang-html">&#60;button class="btn btn--borde" type="button" name="button"&#62;Btn Secundario&#60;/button&#62;</code></pre>
              </div>
            </div>
          </section>
        </div>
        <!--CODIGO-->
      </div>
      <div class="item">
        <div class="title-section">
          <h2>Con &#237;conos</h2>
          <div class="divider"></div>
        </div>
        <p>Los botones con &#237;conos dentro del sitio privado de BancoEstado cumplen la funci&#243;n de reforzar acciones que un usuario puede realizar. Ya sea descargar un archivo, confirmar una operaci&#243;n o enviar un formulario.</p>
        <div class="tipogafria">
          <div class="agrupador-contenido">
            <button class="btn btn--principal" type="button" name="button"><span class="icono icono--descarga"></span>Btn Icono</button>
            <button class="btn btn--borde" type="button" name="button"><span class="icono icono--descarga"></span>Btn icono secundario</button>
          </div>
        </div>
        <!--CODIGO-->
        <div class="wrapper">
          <ul class="tabs clearfix" data-tabgroup="btn-icono">
            <li><a href="#tab5"><span class="texto-desplegable">html</span><span class="icon-elem icon-elem--arrow_drop_down"></span></a></li>
          </ul>
          <section id="btn-icono" class="tabgroup close">
            <div id="tab5">
              <div class="code-box">
                <div class="code-box__button">
                  <button id="buttonCopy" class="button code-box__copy" type="button" onclick="copyToClipboard('#cp-btn-icono')"><span class="icon-elem icon-elem--content_copy"></span>Copiar</button>
                </div>
                <pre id="cp-btn-icono" class="prettyprint linenums"><code class="lang-html">&#60;button class="btn btn--principal" type="button" name="button"&#62;
  &#60;span class="icono icono--descarga&#62;&#60;/span&#62;Btn Icono
&#60;/button&#62;<hr>
&#60;button class="btn btn--borde" type="button" name="button"&#62;
  &#60;span class="icono icono--descarga&#62;&#60;/span&#62;Btn Icono
&#60;/button&#62;</code></pre>
              </div>
            </div>
          </section>
        </div>
        <!--CODIGO-->
      </div>
      <div class="item">
        <div class="title-section">
          <h2>Tama&#241;os</h2>
          <div class="divider"></div>
        </div>
        <p>Cada bot&#243;n presente en esta gu&#237;a de estilos digitales cuenta con un tamaño est&#225;ndar que le asegura al usuario una correcta experiencia y facilita la navegaci&#243;n.</p>
        <div class="tipogafria">
          <div class="agrupador-contenido">
            <button class="btn btn--principal btn--mini" type="button" name="button">Btn principal mini</button>
            <button class="btn btn--borde btn--mini" type="button" name="button">Btn secundario mini</button>
          </div>
        </div>
        <!--CODIGO-->
        <div class="wrapper">
          <ul class="tabs clearfix" data-tabgroup="btn-tamaños">
            <li><a href="#tab7"><span class="texto-desplegable">html</span><span class="icon-elem icon-elem--arrow_drop_down"></span></a></li>
            <li><a href="#tab8"><span class="texto-desplegable">css</span><span class="icon-elem icon-elem--arrow_drop_down"></span></a></li>
          </ul>
          <section id="btn-tamaños" class="tabgroup close">
            <div id="tab7">
              <div class="code-box">
                <div class="code-box__button">
                  <button id="buttonCopy" class="button code-box__copy" type="button" onclick="copyToClipboard('#cp-btn-tamano')"><span class="icon-elem icon-elem--content_copy"></span>Copiar</button>
                </div>
                <pre id="cp-btn-tamano" class="prettyprint linenums"><code class="lang-html">&#60;button class="btn btn--principal btn-mini" type="button" name="button"&#62;
  &#60;span class="icon-elem icon-elem--file_download"&#62;&#60;/span&#62;Btn principal mini
&#60;/button&#62;<hr>
&#60;button class="btn btn--borde btn-mini" type="button" name="button"&#62;
  &#60;span class="icon-elem icon-elem--file_download"&#62;&#60;/span&#62;Btn secundario mini
&#60;/button&#62;</code></pre>
              </div>
            </div>
            <div id="tab8">
              <div class="code-box">
                <div class="css-modificadores">
                  <h6>Modificadores de estilos</h6>
                  <ul class="listado-clases">
                    <li><code class="clases">btn--mini</code></li>
                  </ul>
                </div>
              </div>
            </div>
          </section>
        </div>
        <!--CODIGO-->
      </div>
    </section>
  </div>
  <?php include 'footer.php';?>
</main>
