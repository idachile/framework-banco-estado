<?php include 'header.php';?>
<main>
  <div class="portada">
    <div class="portada__titulo">
      <h1 class="dos">Manual de Estilos y est&#225;ndares</h1>
    </div>
  </div>
  <div class="content-box">
    <div class="contenido">
      <!--introduccion -->
      <div class="horizon horizon--top gridle-row introduccion">
        <div class="gridle-gr-8">
          <p>Bienvenidos a la gu&#237;a de estilos digitales de BancoEstado. Su objetivo es estandarizar y fijar los criterios de referencia para el desarrollo de interfaces en el sitio privado.</p>
          <p>Al crear nuevas p&#225;ginas y funcionalidades es clave mantener estas definiciones, ya que su finalidad es resguardar y no afectar la experiencia de los usuarios.</p>
        </div>
        <div class="gridle-gr-4 gridle-gr-12@tablet">
          <div class="boton-descarga">
            <button class="btn btn--principal"><span class="icono icono--descarga"></span>Descargar pack</button>
          </div>
          <p class="tag"><small>Contiene documento html,css y javascript</small></p>
        </div>
      </div>
      <!--introduccion -->
      <section class="horizon pilares">
        <div class="title-section">
          <h2>Pilares</h2>
          <div class="divider"></div>
        </div>
        <div class="gridle-row card">
          <div class="gridle-gr-4 gridle-gr-12@tablet">
            <article class="common-box">
              <figure class="common-box__figure">
  							<img src="images/iconos/servicios.svg">
              </figure>
              <div class="common-box__body">
                <h3>Servicios</h3>
                <p>Realizar una transacci&#243;n o solicitar asistencia requiere un flujo l&#243;gico y f&#225;cil de comprender. Cada elemento debe cumplir la funci&#243;n correcta y ser ubicado donde no dificulte su uso.</p>
              </div>
            </article>
          </div>
          <div class="gridle-gr-4 gridle-gr-12@tablet">
            <article class="common-box">
              <figure class="common-box__figure">
    						<img src="images/iconos/adaptable.svg">
              </figure>
              <div class="common-box__body">
                <h3>Adaptables</h3>
                <p>No importa si se accede desde un computador o smartphone. Su funcionalidad no debe verse afectada por el tipo de dispositivo usado o elementos como el tamaño de pantalla.</p>
              </div>
            </article>
          </div>
          <div class="gridle-gr-4 gridle-gr-12@tablet">
            <article class="common-box">
              <figure class="common-box__figure">
    						<img src="images/iconos/centrado.svg">
              </figure>
              <div class="common-box__body">
                <h3>Centrado en Usuario</h3>
                <p>Toda p&#225;gina o funcionalidad implementada debe permitir al usuario operar f&#225;cilmente. Debe permitir el acceso al contenido, opciones e interactuar con ellos sin complicaciones.</p>
              </div>
            </article>
          </div>
        </div>
      </section>
      <section class="guias">
        <div class="title-section">
          <h2>Gu&#237;as</h2>
          <div class="divider"></div>
        </div>
        <div class="gridle-row card">
          <div class="gridle-gr-6 gridle-gr-12@tablet">
            <article class="common-box common-box--horizontal">
              <figure class="common-box__figure">
  							<img src="images/iconos/interfaz.svg">
              </figure>
              <div class="common-box__body">
                <h3>Interfaz</h3>
                <p>Cada elemento que forme parte del sitio privado debe mantener los principios de la gu&#237;a de estilos digitales. Colores, tamaños e incluso ubicaci&#243;n tienen que respetar las pautas establecidas.</p>
                <ul class="mini-nav">
                  <li><a href="colores.php">Color</a></li>
                  <li><a href="tipografia.php">Tipograf&#237;as</a></li>
                  <li><a href="botones.php">Botones</a></li>
                  <li><a href="contenedores.php">Contenedores</a></li>
                  <li><a href="formularios.php">Formularios</a></li>
                  <li><a href="tablas.php">Tablas y listas</a></li>
                  <li><a href="alertas.php">Alertas</a></li>
                </ul>
              </div>
            </article>
          </div>
          <div class="gridle-gr-6 gridle-gr-12@tablet">
            <article class="common-box common-box--horizontal">
              <figure class="common-box__figure">
    					 <img src="images/iconos/redaccion.svg">
              </figure>
              <div class="common-box__body">
                <h3>Redacci&#243;n</h3>
                <p>Los contenidos tambi&#233;n responden a los criterios de este manual. Su coherencia y armon&#237;a con otros elementos del sitio privado son necesarias para entregar una positiva experiencia de usuario.</p>
              </div>
            </article>
          </div>
        </div>
      </section>
      <!--<section class="como-usar">
        <div class="title-section">
          <h2>C&#243;mo utilizar la librer&#237;a si...</h2>
          <div class="divider"></div>
        </div>
        <div class="gridle-row card">
          <div class="gridle-gr-4 gridle-gr-12@tablet">
            <article class="common-box">
              <figure class="common-box__figure">
  							<img src="images/iconos/ux.svg">
              </figure>
              <div class="common-box__body">
                <h3>Soy Diseñador UX/UI</h3>
                <p>La gu&#237;a de estilos digitales contiene los lineamientos para implementar soluciones en el sitio privado pensando en la experiencia de usuario. Las interfaces tambi&#233;n deben responder a estos par&#225;metros.</p>
              </div>
            </article>
          </div>
          <div class="gridle-gr-4 gridle-gr-12@tablet">
            <article class="common-box">
              <figure class="common-box__figure">
    						<img src="images/iconos/desarrollador.svg">
              </figure>
              <div class="common-box__body">
                <h3>Soy Desarrollador</h3>
                <p>El c&#243;digo requiere ser tratado de tal forma que se asegure su orden. Cada soluci&#243;n y componente debe ser correctamente clasificado, organizado y su funcionamiento no debe afectar a otros elementos.</p>
              </div>
            </article>
          </div>
          <div class="gridle-gr-4 gridle-gr-12@tablet">
            <article class="common-box">
              <figure class="common-box__figure">
    						<img src="images/iconos/contenidos.svg">
              </figure>
              <div class="common-box__body">
                <h3>Hago contenidos</h3>
                <p>Cada texto en el sitio privado responde a ciertas particularidades. Ya sea un t&#237;tulo, p&#225;rrafo o n&#241;meros, tienen que seguir los par&#225;metros ya establecidos en esta gu&#237;a de estilos digitales.</p>
              </div>
            </article>
          </div>
        </div>
      </section>-->
    <!--  <section class="ayuda">
        <div class="title-section">
          <h2>¿Dudas?</h2>
          <div class="divider"></div>
        </div>
        <div class="gridle-row">
          <div class="gridle-gr-6 gridle-gr-12@tablet">
            <p>¿Tienes dudas? Revisa nuestros canal de Preguntas Frecuentes o cont&#225;ctanos mediante el Formulario.</p>
          </div>
          <div class="gridle-gr-6 gridle-gr-12@tablet">
            <div class="menu-footer">
              <div class="gridle-row">
                <div class="gridle-gr-6">
                  <a href="#" class="footer-bar__link">soporte@gui.bancoestado.cl</a>
                  <a href="#" class="footer-bar__link">ux@gui.bancoestado.cl</a>
                  <a href="#" class="footer-bar__link">ui@gui.bancoestado.cl</a>
                </div>
                <div class="gridle-gr-6 gridle-gr-12@tablet">
                  <a href="#" class="footer-bar__link">desarrollo@gui.bancoestado.cl</a>
                  <a href="#" class="footer-bar__link">contenidos@gui.bancoestado.cl</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>-->
    </div>
  </div>
  <?php include 'footer.php';?>
</main>
