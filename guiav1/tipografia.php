<?php include 'header.php';?>
<main>
  <section class="content breadcrumbs-bar-frame">
    <?php include 'partials/breadcrumbs.php';?>
  </section>
  <section class="content">
  <div class="introduccion">
    <div class="title-section">
      <h1>Tipograf&#237;a</h1>
    </div>
    <p>Es uno de los elementos m&#225;s importantes dentro del sitio privado de BancoEstado. Junto con entregarle al usuario la informaci&#243;n que necesita para operar exitosamente un proceso, facilita la interacci&#243;n con las distintas secciones, no importando el orden en que se realice. Un uso correcto asegura que cada men&#250;, opci&#243;n y t&#237;tulo desarrollado cumpla efectivamente su rol.</p>
  </div>
</section>
  <div class="content-box">
    <section>
      <div class="item">
        <div class="title-section">
          <h2>Cuerpos de texto</h2>
          <div class="divider"></div>
        </div>
        <p>La tipograf&#237;a utilizada es “Open Sans”. Entre sus principales caracter&#237;sticas destaca estar optimizada para web y permitir la legibilidad de los contenidos. Con ella el usuario puede distinguir correcta y r&#225;pidamente las jerarqu&#237;as de un texto: t&#237;tulos, p&#225;rrafos y subt&#237;tulos.</p>
        <div class="tipogafria">
          <p class="tipogafria__name">Open Sans</p>
          <div class="tipogafria__example">
            <p>ABCDEFGHIJKLMNOPQRSTUVWXYZ</p>
            <p>abcdefghijklmnopqrstuvwxyz</p>
            <p>0 1 2 3 4 5 6 7 8 9</<p>
          </div>
        </div>
        <?php //include 'partials/tabs_code.php' ?>
      </div>
      <div class="item">
        <div class="title-section">
          <h2>Tamaños</h2>
          <div class="divider"></div>
        </div>
        <p>Los tamaños definidos en esta gu&#237;a de estilos digitales buscan asegurar la experiencia de usuario en el sitio privado. El contenido debe ser visualizado correctamente en diversas plataformas, por lo que nuevos desarrollos deben considerar &#250;nicamente estos par&#225;metros.</p>
          <div class="tipogafria">
            <div class="tipogafria--openSans">
              <p class="tipogafria--openSans tamano--beast">Tamaño fuente 40px</p>
              <p class="tipogafria--openSans tamano--double">Tamaño fuente 32px</p>
              <p class="tipogafria--openSans tamano--tallest">Tamaño fuente 28px</p>
              <p class="tipogafria--openSans tamano--midtall">Tamaño fuente 24px</p>
              <p class="tipogafria--openSans tamano--big">Tamaño fuente 20px</p>
              <p class="tipogafria--openSans tamano--regular">Tamaño fuente 16px</p>
              <p class="tipogafria--openSans tamano--tiny">Tamaño fuente 14px</p>
              <p class="tipogafria--openSans tamano--mini">Tamaño fuente 12px</p>
            </div>
          </div>
          <?php //include 'partials/tabs_code.php' ?>
      </div>
      <div class="item">
        <div class="title-section">
          <h2>Pesos</h2>
          <div class="divider"></div>
        </div>
        <p>Su utilizaci&#243;n depender&#225; de la ubicaci&#243;n y funci&#243;n que cumplir&#225; un texto determinado dentro de la plataforma. Ya sea entregar informaci&#243;n relevante al usuario, indicar alguna acci&#243;n o el resultado de un proceso, el contenido debe ser legible bas&#225;ndose en estos par&#225;metros.</p>
          <div class="tipogafria grosores">
            <div class="tipogafria--openSans">
              <p class="tipogafria--openSans tamano--beast grosor--bold">Bold: 700</p>
              <p class="tipogafria--openSans tamano--beast grosor--regular">Regular: 400</p>
            </div>
          </div>
        <?php //include 'partials/tabs_code.php' ?>
      </div>
      <div class="item">
        <div class="title-section">
          <h2>Estilos de Texto</h2>
          <div class="divider"></div>
        </div>
        <p>Los texto dentro del sitio privado de BancoEstado facilitan la entrega de informaci&#243;n al usuario. Tambi&#233;n permiten desplegar los datos resultantes de alguna operaci&#243;n espec&#237;fica. Para jerarquizar la forma de visualizar los contenidos es necesario seguir las siguientes pautas.</p>
          <div class="title-section">
              <h3>T&#237;tulos</h3>
            <div class="divider"></div>
          </div>
          <p>Al construir un texto es uno de los elementos que cobra mayor importancia. El t&#237;tulo tiene que ser capaz de entregar una primera aproximaci&#243;n al contenido. De la misma forma los subt&#237;tulos permiten ordenar la informaci&#243;n de forma l&#243;gica.</p>
          <div class="tipogafria">
            <div class="headings">
              <div class="heads">
                <h1>h1.Heading</h1>
                <p class="detail tamano--medium">32px <span>(16 x 2)</span> Open Sans, line-height 1.3</p>
              </div>
              <div class="heads">
                <h2>h2.Heading</h2>
                <p class="detail tamano--medium">28px <span>(16 x 1.75)</span> Open Sans, line-height 1.3</p>
              </div>
              <div class="heads">
                <h3>h3.Heading</h3>
                <p class="detail tamano--medium">24px <span>(16 x 1.5)</span> Open Sans, line-height 1.3</p>
              </div>
              <div class="heads">
                <h4>h4.Heading</h4>
                <p class="detail tamano--medium">20px <span>(16 x 1.75)</span> Open Sans, line-height 1.3</p>
              </div>
              <div class="heads">
                <h5>h5.Heading</h5>
                <p class="detail tamano--medium">18px <span>(16 x 1.125)</span> Open Sans, line-height 1.3</p>
              </div>
              <div class="heads">
                <h6>h6.Heading</h6>
                <p class="detail tamano--medium">16px <span>(16 x 1)</span> Open Sans, line-height 1.3</p>
              </div>
            </div>
          </div>
        <div class="title-section">
          <h3>P&#225;rrafos</h3>
          <div class="divider"></div>
        </div>
        <p>Es donde se concentrar&#225; la informaci&#243;n. Su extensi&#243;n depender&#225; del contenido y su ubicaci&#243;n al interior del sitio privado. Tambi&#233;n debe tener la capacidad de adaptarse a diversos dispositivos y tamaños.</p>
        <div class="tipogafria">
          <div class="box-parrafos">
            <div class="box-parrafos--small">
              <h5>Small</h5>
              <p><small>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Temporibus hic similique voluptas nam praesentium et numquam ratione, vel non autem excepturi distinctio perspiciatis sunt deserunt nulla minus voluptatum libero ad odit nesciunt? Sunt
                autem cupiditate, incidunt nihil similique facere, eos cum deserunt dolore esse, aliquid eligendi modi unde libero rerum.</small></p>
            </div>
            <div class="box-parrafos--tiny">
              <h5>Tiny</h5>
              <p><small>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Temporibus hic similique voluptas nam praesentium et numquam ratione, vel non autem excepturi distinctio perspiciatis sunt deserunt nulla minus voluptatum libero ad odit nesciunt? Sunt
                autem cupiditate, incidunt nihil similique facere, eos cum deserunt dolore esse, aliquid eligendi modi unde libero rerum.</small></p>
            </div>
            <div class="box-parrafos--strong">
              <h5>Strong</h5>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. <strong>Temporibus hic similique voluptas nam praesentium</strong>et numquam ratione, vel non autem excepturi distinctio perspiciatis sunt deserunt nulla minus voluptatum libero ad odit nesciunt? Sunt
                autem cupiditate, incidunt nihil similique facere, eos cum deserunt dolore esse, aliquid eligendi modi unde libero rerum.</p>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  <?php include 'footer.php'; ?>
</main>
