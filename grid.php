<?php include 'header.php';?>
<main>
  <div class="title-page title-page--ghost">
    <h1>Grilla</h1>
    <div class="boton-descarga">
      <a href="#" class="button button--secondary button--full-width"><span class="icon-elem icon-elem--arrow_downward"></span>Descarga este recurso</a>
    </div>
  </div>
  <div class="other-nav">
    <ul class="menu-other">
      <li class="menu-item menu-item__link">
        <a href="tipo-page.shtml">Secci&#243;n 1</a>
      </li>
      <li class="menu-item menu-item__link">
        <a href="#">Secci&#243;n 2</a>
      </li>
      <li class="menu-item menu-item__link">
        <a href="#">Secci&#243;n 3</a>
      </li>
    </ul>
  </div>
  <div class="content-box">
    <section class="horizon--top">
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Temporibus hic similique voluptas nam praesentium et numquam ratione, vel non autem excepturi distinctio perspiciatis sunt deserunt nulla minus voluptatum libero ad odit nesciunt? Sunt autem
        cupiditate, incidunt nihil similique facere, eos cum deserunt dolore esse, aliquid eligendi modi unde libero rerum.</p>
      <div class="item">
        <div class="title-section">
          <h2>Lorem Ipsum</h2>
          <div class="divider"></div>
        </div>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Temporibus hic similique voluptas nam praesentium et numquam ratione, vel non autem excepturi distinctio perspiciatis sunt deserunt nulla minus voluptatum libero ad odit nesciunt? Sunt
          autem cupiditate, incidunt nihil similique facere, eos cum deserunt dolore esse, aliquid eligendi modi unde libero rerum.</p>
        <div class="ejemplo">
          <a href="#" class="button button--secondary button--demo"><span class="icon-elem icon-elem--arrow_downward"></span>Demo</a>
          <img src="images/grilla/grilla-ejemplo.jpg" alt="grilla de ejemplo">
        </div>
        <div class="cd-tabs">
          <nav>
            <ul class="cd-tabs-navigation">
              <li><a data-content="html" class="selected" href="#0">HTML</a></li>
              <li><a data-content="css" href="#0">CSS</a></li>
              <li><a data-content="javascript" href="#0">javaScript</a></li>
            </ul>
            <!-- cd-tabs-navigation -->
          </nav>
          <ul class="cd-tabs-content">
            <li data-content="html" class="selected">
              <div class="content-box code-box code-box--html">
                <p class="code-box__line"> &lt;<span class="code-box__tag">div</span> <span class="code-box__class">class="<span class="code-box__content-class">caja-texto</span>"</span>&gt;</p>
                <p class="code-box__line code-box--children-first"> &lt;<span class="code-box__tag">h2</span>&gt;HTML&lt;/<span class="code-box__tag">h2</span>&gt;</p>
                <p class="code-box__line code-box--children-first"> &lt;<span class="code-box__tag">p</span>&gt;Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quibusdam, voluptate officiis reiciendis numquam repudiandae, velit quod assumenda ratione eligendi provident?&lt;/<span class="code-box__tag">p</span>&gt;</p>
                <p class="code-box__line"> &lt;/<span class="code-box__tag">div</span>&gt;</p>
              </div>
            </li>
            <li data-content="css">
              <div class="content-box content-box--grey code-box code-box--html">
                <p class="code-box__line"> &lt;<span class="code-box__tag">div</span> <span class="code-box__class">class="<span class="code-box__content-class">caja-texto</span>"</span>&gt;</p>
                <p class="code-box__line code-box--children-first"> &lt;<span class="code-box__tag">h2</span>&gt;CSS&lt;/<span class="code-box__tag">h2</span>&gt;</p>
                <p class="code-box__line code-box--children-first"> &lt;<span class="code-box__tag">p</span>&gt;Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quibusdam, voluptate officiis reiciendis numquam repudiandae, velit quod assumenda ratione eligendi provident?&lt;/<span class="code-box__tag">p</span>&gt;</p>
                <p class="code-box__line"> &lt;/<span class="code-box__tag">div</span>&gt;</p>
              </div>
            </li>
            <li data-content="javascript">
              <div class="content-box content-box--grey code-box code-box--html">
                <p class="code-box__line"> &lt;<span class="code-box__tag">div</span> <span class="code-box__class">class="<span class="code-box__content-class">caja-texto</span>"</span>&gt;</p>
                <p class="code-box__line code-box--children-first"> &lt;<span class="code-box__tag">h2</span>&gt;CSS&lt;/<span class="code-box__tag">h2</span>&gt;</p>
                <p class="code-box__line code-box--children-first"> &lt;<span class="code-box__tag">p</span>&gt;Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quibusdam, voluptate officiis reiciendis numquam repudiandae, velit quod assumenda ratione eligendi provident?&lt;/<span class="code-box__tag">p</span>&gt;</p>
                <p class="code-box__line"> &lt;/<span class="code-box__tag">div</span>&gt;</p>
              </div>
            </li>
          </ul>
        </div>
      </div>
    </section>
  </div>
<?php include 'footer.php';?>
</main>
